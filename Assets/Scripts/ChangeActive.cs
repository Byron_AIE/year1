﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeActive : MonoBehaviour
{
    [SerializeField] GameObject targetObject = null;

    public void Deactivate()
    {
        targetObject.SetActive(false);
    }

    public void Activate()
    {
        targetObject.SetActive(true);
    }

}
