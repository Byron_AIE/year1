﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public static uint attackingEnemies = 0u;
    public static bool CanEnemyAttack { get; private set; } = true;
    private static float attackCounter = 0f;
    private const float timeBetweenAttacks = 1f;

    private void Start()
    {
        CanEnemyAttack = true;
    }

    private void Update()
    {
        // if no enemies are attacking then start the countdown
        if (attackingEnemies > 0u && CanEnemyAttack)
        {
            StartCoroutine(Countdown());
        }
    }

    private IEnumerator Countdown()
    {
        CanEnemyAttack = false;
        attackCounter = timeBetweenAttacks;
        while (true)
        {
            attackCounter -= Time.deltaTime;
            if (attackCounter <= 0)
            {
                CanEnemyAttack = true;
                yield break;
            }
            yield return null;
        }
    }
}
