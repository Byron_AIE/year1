﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct AudioContainer
{
    public string AccessString;
    public AudioClip[] AudioClips;
}

public class AudioManager : GenericSingleton<AudioManager>
{
    [SerializeField] List<AudioContainer> AudioContainers;
    private Dictionary<string, AudioClip[]> audioClipsDict;
    
    // Start is called before the first frame update
    void Start()
    {
        audioClipsDict = new Dictionary<string, AudioClip[]>(AudioContainers.Count);

        if (AudioContainers != null || AudioContainers.Count == 0)
        {
            foreach (AudioContainer AC in AudioContainers)
            {
                audioClipsDict.Add(AC.AccessString, AC.AudioClips);
            }
        }
        else
            Debug.LogError("AudioContainerList is empty");
    }

    public AudioClip GetRandomSound(string clipSet)
    {
        AudioClip clip = null;
        clip = audioClipsDict[clipSet][UnityEngine.Random.Range(0, audioClipsDict[clipSet].Length)];

        if (clip != null)
        {
            return clip;
        }
        else
        {
            Debug.LogError("AudioClip accessed at: " + clipSet + " is null");
            return null;
        }
    }

    public AudioClip GetUniqueSound(string clipSet)
    {
        AudioClip clip = null;
        var audioClipSet = audioClipsDict[clipSet];

        int n = Random.Range(1, audioClipSet.Length);
        clip = audioClipSet[n];

        audioClipSet[n] = audioClipSet[0];
        audioClipSet[0] = clip;

        if (clip)
            return clip;
        else
            Debug.LogError("AudioClip accessed at: " + clipSet + " is null");

        return null;
    }
}
