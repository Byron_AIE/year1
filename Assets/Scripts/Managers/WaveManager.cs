﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


[System.Serializable]
public struct TutorialLevel
{
    [System.Serializable]
    public struct TutorialEnemyData
    {
        [Range(0, 20)]
        public float m_SpawnWeighting;
        public WaveManager.EnemyTypes m_EnemyType;
    }

    [System.Serializable]
    public struct IntervalTutorialEnemyData
    {
        [Range(0, 100)]
        public int m_SpawnInterval;
        public WaveManager.EnemyTypes m_EnemyType;
    }

    [Range(0, 500)]
    public int m_NumberToSpawn;
    public UnityEvent m_LevelEvent;
    public List<TutorialEnemyData> m_TutStandardSpawnEnemyData;
    public List<IntervalTutorialEnemyData> m_TutIntervalEnemyData;
}

public class WaveManager : GenericSingleton<WaveManager>
{    
    [HideInInspector]
    public uint m_TotalEnemiesInitialised { get; private set; } = 0;
    public int  m_WaveNumber { get; private set; } = 0;
    public bool m_AllEnemiesSpawned = false;
    public bool m_PlayTutorial = false;
    [SerializeField] SkillTree m_SkillTree;

    private List<Enemy> m_enemyList;
    private List<Enemy> m_deadEnemies;

    private uint  m_activeEnemies = 0u;
    private float m_totalSpawnWeighting;
    private int   m_enemySpawnNum;
    [HideInInspector] public uint m_totalSpawnedEnemies { get; private set; } = 0;

    // TutLevels
    private int  m_tutLevelIter = 0;
    private int  m_totalTutLevels = 0;

    private struct TemporarySpawnDataContainer
    {
        public int m_EnemySpawnNum;
        public List<EnemyStandardContainer> m_StandardSpawnData;
        public List<EnemyIntervalContainer> m_IntervalSpawnData;
    }

    [Header("Level Data")]
    [SerializeField] [Range(0, 25)] uint m_LevelMidWaitTime = 10u;
    [SerializeField] [Range(0, 10)] uint m_LevelStartWaitTime = 5u;   
    [SerializeField] [Range(0, 10)] float m_EndWaveWaitTime = 1f;

    [Header("Enemy Data")]
    [SerializeField] [Range(0, 5000)] int m_StartEnemySpawnNum = 100;
    [Header("Every X amount of waves increment enemy stats")]
    [SerializeField] [Range(1, 10)] int   m_EnemyWaveUpgrades = 2;
    [SerializeField] [Range(0, 2f)] float m_MinSpawnDelay = .1f;
    [SerializeField] [Range(0, 2f)] float m_MaxSpawnDelay = .5f;
    [SerializeField] [Range(1, 5000)] int m_MaxEnemiesInLevel = 5000;
    [SerializeField] [Range(1, 500)] int  m_MaxEnemiesOnScreen = 30;
    [SerializeField] [Range(1, 100)] int  m_EnemyFrameSpawnCount = 10;
    [SerializeField] [Range(0, 100)] int  m_MaxCorpseLimit = 20;
    [SerializeField] [Range(1, 100)] int  m_NumberOfNewEnemiesPerWave = 7;
    [SerializeField] [Range(1, 20)] int   m_MusicFadeEnemyNum = 5;
    public UnityEvent m_musicFade = null;

    public UnityEvent m_WaveCompleteEvent;
    private AudioSource m_waveManagerAS;

    [SerializeField] Collider[] m_SpawnLocations = null;
    private Bounds[] m_spawnBounds = null;

    // Move outside of wavemanager completely, leave on prefabs only
    // or move to SO's

    // get rid of container classes

    // if SO's are overriden while in build and application crashed and SO wasn't reset, have a bool which is set once reset is finished
    // if false then reset values on next startup with defaults

    // make new SO which will contain prefab for enemy and setup data
    // holds onto default data as well to swap to defaults at player death or application exit

    [Header("Supply prefab along with default values")]
    [SerializeField] List<EnemyStandardContainer> m_EnemyData = null;

    [Header("Use spawninterval instead of weighting for spawn frequency")]
    [SerializeField] List<EnemyIntervalContainer> m_IntervalEnemyData = null;

    [Header("Size of tutorialLevelData dictates how many tutorial levels there are")]
    [Header("Data for tutorial levels")]
    [SerializeField] TutorialLevel[] m_TutorialLevelData = null;

    private TemporarySpawnDataContainer m_defaultSpawnData;

    [System.Serializable]
    public enum EnemyTypes
    {
        Melee,
        Ranger,
        Flagbearer,
        Kamikaze,
        Agile
    }
   
    private void Awake()
    {
        if (!m_SkillTree)
            Debug.LogWarning("Skilltree not assigned on wavemanager");

        m_enemyList = new List<Enemy>(m_MaxEnemiesInLevel);
        m_deadEnemies = new List<Enemy>(m_MaxCorpseLimit);
        if (m_SpawnLocations != null)
            m_spawnBounds = new Bounds[m_SpawnLocations.Length];

        if (m_SpawnLocations != null)
        {
            for (int i = 0; i < m_SpawnLocations.Length; i++)
            {
                Collider c = m_SpawnLocations[i];
                if (c != null)
                {
                    m_spawnBounds[i] = c.bounds;
                }
                else
                {
                    Debug.LogError("Spawn location collider (" + m_SpawnLocations[i].name + ") is null, and will not spawn enemies.");
                }
            }
        }
        else
            Debug.LogError("Spawnlocations is unfilled");
    }

    void Start()
    {
        m_waveManagerAS = GetComponent<AudioSource>();
        m_waveManagerAS.ignoreListenerPause = true;
        m_totalTutLevels = m_TutorialLevelData.Length;
        m_enemySpawnNum = m_StartEnemySpawnNum;

        if (m_EnemyData.Contains(null) || m_IntervalEnemyData.Contains(null))
        {
            Debug.LogError("Unfilled spots in enemyData");
        }

        // perform copy of default values to replace later, replace solution with SO or local values if possible
        m_defaultSpawnData.m_EnemySpawnNum = m_StartEnemySpawnNum;
        m_defaultSpawnData.m_StandardSpawnData = new List<EnemyStandardContainer>(m_EnemyData.Count);
        m_defaultSpawnData.m_IntervalSpawnData = new List<EnemyIntervalContainer>(m_IntervalEnemyData.Count);

        foreach (EnemyStandardContainer container in m_EnemyData)
        {
            EnemyStandardContainer spawnContainer = new EnemyStandardContainer(container);
            m_defaultSpawnData.m_StandardSpawnData.Add(spawnContainer);
        }

        foreach (EnemyIntervalContainer container in m_IntervalEnemyData)
        {
            EnemyIntervalContainer spawnContainer = new EnemyIntervalContainer(container);
            m_defaultSpawnData.m_IntervalSpawnData.Add(spawnContainer);
        }
    }

    private void Update()
    {
#if UNITY_EDITOR 
        if (Input.GetKeyDown(KeyBinds.KillAll))
        {
            KillAllEnemies();
        }
#endif
    }

#if UNITY_EDITOR
    public void KillAllEnemies()
    {
        for (int i = 0; i < m_enemyList.Count; i++)
        {
            if (m_enemyList[i].isActiveAndEnabled)
                m_enemyList[i].TakeDamage(10000);
        }     
    }
#endif

    /// <summary>
    /// Attempt to change spawn values - which if successful will start initializing enemies
    /// </summary>
    /// <returns></returns>
    public bool LoadEnemies()
    {
        if (m_enemySpawnNum > m_MaxEnemiesInLevel)
        {
            Debug.LogError("Spawn number exceeds limit on maximum enemies in level - Exiting level load");
            return false;
        }

        if (m_PlayTutorial)
        {
            if (m_totalTutLevels == 0 || m_TutorialLevelData == null)
            {
                Debug.LogError("Tutorial cannot be played: Tutorialdata is empty");
                m_PlayTutorial = false;
                return false;
            }               
            
            // if finished last tutorial level then exit tutorials and replace default spawn data appropriately
            if (m_tutLevelIter == m_totalTutLevels)
            {
                m_PlayTutorial = false;
                ResetEnemySpawnData();             
            }
        }

        m_AllEnemiesSpawned = false;

        ChangeSpawnValues();
        StartCoroutine(InitialiseEnemies());
        return true;
    }

    /// <summary>
    /// Change level spawn values for enemy spawning
    /// </summary>
    private void ChangeSpawnValues()
    {
        // if playing tutorial override values for spawning
        if (m_PlayTutorial)
        {
            // reset values for overwriting
            foreach (EnemyStandardContainer container in m_EnemyData)            
                container.SpawnWeighting = 0f;            

            foreach (EnemyIntervalContainer container in m_IntervalEnemyData)            
                container.SpawnInterval = 0;
            

            var levelData = m_TutorialLevelData[m_tutLevelIter];

            m_enemySpawnNum = levelData.m_NumberToSpawn;
            foreach (TutorialLevel.TutorialEnemyData weightValues in levelData.m_TutStandardSpawnEnemyData)
            {
                m_totalSpawnWeighting += weightValues.m_SpawnWeighting;                
            }           

            // override spawn values for spawning
            foreach(EnemyStandardContainer container in m_EnemyData)
            {
                foreach(TutorialLevel.TutorialEnemyData weightData in levelData.m_TutStandardSpawnEnemyData)
                {
                    if (weightData.m_EnemyType == container.EnemyType)
                    {
                        container.SpawnWeighting = weightData.m_SpawnWeighting;
                    }
                }                                  
            }             
            foreach(EnemyIntervalContainer container in m_IntervalEnemyData)
            {
                foreach (TutorialLevel.IntervalTutorialEnemyData intervalData in levelData.m_TutIntervalEnemyData)
                {
                    if (intervalData.m_EnemyType == container.EnemyType)
                    {
                        container.SpawnInterval = intervalData.m_SpawnInterval;
                    }
                }
            }

            m_tutLevelIter++;
        }

        // not playing tutorials
        else
        {
            if (m_EnemyData != null && m_EnemyData.Count != 0)
            {
                // acquire totalSpawnWeighting for spawning
                foreach (EnemyStandardContainer container in m_EnemyData)
                {
                    m_totalSpawnWeighting += container.SpawnWeighting;
                }
                      
                if (m_WaveNumber != 0)
                {
                    m_enemySpawnNum += m_NumberOfNewEnemiesPerWave;
                    if (m_enemySpawnNum > m_MaxEnemiesInLevel)
                        m_enemySpawnNum = m_MaxEnemiesInLevel;

                    UpgradeEnemies();
                }               
            }
            else
            {
                Debug.LogError("Enemydata is empty or null");
                return;
            }
        }
    }

    private void UpgradeEnemies()
    {
        if (IsDivisible(m_WaveNumber + 1, m_EnemyWaveUpgrades))
        {
            foreach (EnemyStandardContainer container in m_EnemyData)
            {
                container.UpgradeStats();
            }
            foreach (EnemyIntervalContainer container in m_IntervalEnemyData)
            {
                container.UpgradeStats();
            }
        }
    }


    /// <summary>
    /// Add enemy reference to deadCorpse list and exit level if num of enemies defeated = number of enemies spawned
    /// </summary>
    /// <param name="deadEnemy"></param>
    public void AddCorpse(Enemy deadEnemy)
    {
        Debug.Assert(deadEnemy != null);

        if (deadEnemy != null)
        {
            m_deadEnemies.Add(deadEnemy);
            m_activeEnemies--;
            if (m_totalSpawnedEnemies == m_enemySpawnNum && m_activeEnemies == 0)
            {
                StartCoroutine(EndWave());
            }
        }

        if (m_deadEnemies.Count > m_MaxCorpseLimit)
        {
            m_deadEnemies[0].gameObject.SetActive(false);
            m_deadEnemies.RemoveAt(0);
        }

        if (m_enemySpawnNum - m_totalSpawnedEnemies <= m_MusicFadeEnemyNum)
        {
            if (m_musicFade != null)
            {
                m_musicFade.Invoke();
            }
        }
    }  

    public void StartWave()
    {       
        m_totalSpawnedEnemies = 0;
        m_activeEnemies = 0;
        if (!m_PlayTutorial)
            ++m_WaveNumber;
        StartCoroutine(SpawnEnemies());
    }

    public IEnumerator EndWave()
    {
        DespawnAllEnemies();
        m_SkillTree.UnlockNodes();
        yield return new WaitForSeconds(m_EndWaveWaitTime);
        m_WaveCompleteEvent.Invoke();
    }

    public void RestartWaves()
    {
        DespawnAllEnemies();
        ResetEnemySpawnData();
        m_WaveNumber = 0;
    }

    private void DespawnAllEnemies()
    {
        for (int i = 0; i < m_TotalEnemiesInitialised; i++)
        {
            Destroy(m_enemyList[i].gameObject);
        }
        m_enemyList.Clear();
        m_deadEnemies.Clear();

        m_AllEnemiesSpawned = false;
        m_totalSpawnWeighting = 0f;
        m_TotalEnemiesInitialised = 0u;
    }

    private void ResetEnemySpawnData()
    {
        m_enemySpawnNum = m_defaultSpawnData.m_EnemySpawnNum;

        m_EnemyData = new List<EnemyStandardContainer>(m_defaultSpawnData.m_StandardSpawnData);
        m_IntervalEnemyData = new List<EnemyIntervalContainer>(m_defaultSpawnData.m_IntervalSpawnData);
    }
    

    /// <summary>
    /// Returns reference to random instantiated enemy based on ememy spawn weighting values
    /// </summary>
    /// <param name="enemySpawn"></param>
    /// <returns>
    /// Enemy reference - null if failed to instantiate
    /// </returns>
    private Enemy Spawn(bool spawnSpecificClass = false, EnemyIntervalContainer alternateDataContainer = null)
    {
        Enemy enemy = null;
        float randomWeightValue = 0;       

        // Use weighting to spawn random class
        if (!spawnSpecificClass)
        {
            randomWeightValue = UnityEngine.Random.Range(0, m_totalSpawnWeighting);

            for (int i = 0; i < m_EnemyData.Count; i++)
            {
                if (randomWeightValue < m_EnemyData[i].SpawnWeighting)
                {
                    enemy = Instantiate(m_EnemyData[i].Prefab);
                    if (enemy != null)
                    {
                        enemy.Setup(m_EnemyData[i]);
                        return enemy;
                    }
                }
                else
                    randomWeightValue -= m_EnemyData[i].SpawnWeighting;
            }

            Debug.LogError("Enemy spawn failed - Weight spawn failed to return enemy");
            return null;
        }
        else
        {
            enemy = Instantiate(alternateDataContainer.Prefab);
            if (enemy == null)
            {
                Debug.LogError("Specific class spawn failed");
                return null;
            }

            enemy.Setup(alternateDataContainer);
            return enemy;
        }       
    }    

    private bool SpawnIntervalClasses()
    {
        if (m_IntervalEnemyData.Count != 0)
        {
            foreach(EnemyIntervalContainer dataContainer in m_IntervalEnemyData)
            {
                if (IsDivisible(m_enemyList.Count, dataContainer.SpawnInterval))
                {
                    Enemy enemy = Spawn(true, dataContainer);
                    Debug.Log("Spawned: " + dataContainer.Prefab.name + " at " + m_enemyList.Count + 1 + " Enemies");
                    AddEnemy(enemy);

                    if (AreAllEnemiesSpawned())
                    {
                        return true;
                    }
                }
            }            
        }
        return false;
    }

    private void AddEnemy(Enemy enemy)
    {
        m_TotalEnemiesInitialised++;
        m_enemyList.Add(enemy);
    }

    private bool IsDivisible(int x, int j)
    {
        if (j == 0)
            return false;

        return (x % j) == 0;
    }

    private bool AreAllEnemiesSpawned()
    {
        if (m_TotalEnemiesInitialised == m_enemySpawnNum)
        {
            m_AllEnemiesSpawned = true;
            return true;
        }
        return false;
    }


    // Coroutine to begin loading in enemies
    private IEnumerator InitialiseEnemies()
    {
        Enemy enemy = null;

        while(true)
        {                 
            for (int i = 0; i < m_EnemyFrameSpawnCount; i++)
            {
                // If interval class spawn met the limit on how many enemies can be spawned - exit coroutine
                if (SpawnIntervalClasses())          
                    yield break;                

                enemy = Spawn();
                if (enemy == null)
                {
                    Debug.LogError("Spawning enemy failed | Aborting enemies spawn - NULL reference - Are you missing a prefab reference?");
                    yield break;
                }

                AddEnemy(enemy);

                Debug.Log("Initialised enemies: " + m_enemyList.Count + " Out of " + m_enemySpawnNum + " Enemies to spawn");

                if (AreAllEnemiesSpawned())
                    yield break;                
            }

            if (m_enemyList.Count > m_enemySpawnNum)
                Debug.LogError("Initialised enemies exceeds enemyspawn count - Enemylist size: " + m_enemyList.Count + " out of " + m_enemySpawnNum + " to spawn");
           
            yield return null;
        }         
    }


    private IEnumerator SpawnEnemies()
    {      
        for (int i = 0; i < m_enemyList.Count; i++)      
        {
            Enemy enemy = m_enemyList[i];
            enemy.gameObject.SetActive(true);
            Bounds bounds = FindRandomSpawn();
            enemy.SetPosition(RandomPointInBounds(bounds));
            m_totalSpawnedEnemies++;
            m_activeEnemies++;

            // If active enemies meets limit on MaxEnemiesOnScreen then wait until a spot is freed from a dead enemy
            if (m_activeEnemies == m_MaxEnemiesOnScreen)
            {
                yield return new WaitUntil(() => m_activeEnemies < m_MaxEnemiesOnScreen);
            }

            yield return new WaitForSeconds(UnityEngine.Random.Range(m_MinSpawnDelay, m_MaxSpawnDelay));
        }                  
    }

    private Bounds FindRandomSpawn()
    {
        if (m_SpawnLocations != null && m_SpawnLocations.Length != 0)
        {
            return m_spawnBounds[UnityEngine.Random.Range(0, m_spawnBounds.Length)];
        }

        Debug.LogError("Spawnlocation invalid, cannot access spawnlocations: Spawnlocations: " + m_SpawnLocations);
        return new Bounds();
    }


    private static Vector3 RandomPointInBounds(Bounds bounds)
    {
        if (bounds == null)
        {
            Debug.LogError("RP Bounds Is Null");
            return Vector3.zero;
        }

        Vector3 r = new Vector3(
            UnityEngine.Random.Range(bounds.min.x, bounds.max.x),
            bounds.min.y,
            UnityEngine.Random.Range(bounds.min.z, bounds.max.z));

        return r;
    }
}