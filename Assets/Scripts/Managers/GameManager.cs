﻿using System;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

using UnityStandardAssets.Characters.FirstPerson;

public class GameManager : GenericSingleton<GameManager>
{
    private static WaveManager m_waveManager = null;
    private static FirstPersonController player = null;
    private static HUDManager m_hudmanager = null;
    private static Menus m_menu = null;
    private static AudioManager m_audiomanager = null;
    private static PostProcessVolume m_ppVolume = null;
    private static string[] m_EnemyTags;

    public static string m_MeleeEnemyTag = "MeleeEnemy";
    public static string m_RangerEnemyTag = "RangedEnemy";
    public static string m_PlayerTag = "Player";

    public static int m_EnemyLayer = 12;
    public static int m_IgnoreCollisionLayer = 11;
    public static int m_PlayerBlockerLayer = 13;
    public static int m_PlayerLayer = 14;

    public static int PlayerIgnoreLayerMask
    {
        get
        {
            int mask1 = 1 << m_IgnoreCollisionLayer;
            int mask2 = 1 << m_PlayerLayer;
            int finalMask = mask1 | mask2;
            finalMask = ~finalMask;
            return finalMask;
        }
    }

    public static int EnemyIgnoreLayerMask
    {
        get
        {
            int mask1 = 1 << m_EnemyLayer;
            int finalMask = mask1;
            finalMask = ~finalMask;
            return finalMask;
        }
    }
   
    public static void ChangeGlobalPhysicsLayers(ref int[] IgnoreLayers, int CurrentLayer, bool activate)
    {
        for (int i = 0; i < IgnoreLayers.Length; i++)
        {
            Physics.IgnoreLayerCollision(IgnoreLayers[i], CurrentLayer, activate);
        }
    }

    public static T GetPlayerWeapon<T>(Weapons.WeaponTypes type)
    {
        if (player)
        {
            var weapon = player.GetWeapon(type);            
            return (T)Convert.ChangeType(weapon, typeof(T));       
        }
        return (T)Convert.ChangeType(null, typeof(T));
    }   

    public static PostProcessVolume PPVolume
    {
        get
        {
            if (m_ppVolume == null)
            {
                m_ppVolume = FindObjectOfType<PostProcessVolume>();
                if (m_ppVolume == null)
                {
                    Debug.LogError("Cannot find postProcessVolume in scene");
                    return null;
                }
            }
            return m_ppVolume;
        }
    }

    public static FirstPersonController Player
    {
        get
        {
            if (player == null)
            {
                player = FindObjectOfType<FirstPersonController>();
                if (player == null)
                {
                    Debug.LogError("Cannot find player in scene");
                    return null;
                }
            }
            return player;
        }
    }

    public static WaveManager Wavemanager
    {
        get
        {
            if (m_waveManager == null)
            {
                m_waveManager = FindObjectOfType<WaveManager>();
                if (m_waveManager == null)
                {
                    Debug.LogError("Cannot find wavemanager in scene");
                    return null;
                }
            }
            return m_waveManager;
        }
    }

    public static HUDManager Hudmanager
    {
        get
        {
            if (m_hudmanager == null)
            {
                m_hudmanager = GameObject.FindGameObjectWithTag("HUDManager").GetComponent<HUDManager>();
                if (m_hudmanager == null)
                {
                    Debug.LogError("Cannot find hudmanager in scene");
                    return null;
                }
            }
            return m_hudmanager;
        }
    }

    public static Menus Menu
    {
        get
        {
            if (m_menu == null)
            {
                m_menu = GameObject.FindGameObjectWithTag("MenuManager").GetComponent<Menus>();
                if (m_menu == null)
                {
                    Debug.LogError("Cannot find menu in scene");
                    return null;
                }
            }
            return m_menu;
        }
    }

    public static AudioManager Audiomanager
    {
        get
        {
            if (m_audiomanager == null)
            {
                m_audiomanager = GameObject.FindGameObjectWithTag("AudioManager").GetComponent<AudioManager>();
                if (m_audiomanager == null)
                {
                    Debug.LogError("Cannot find audiomanager in scene");
                    return null;
                }
            }
            return m_audiomanager;
        }
    }

    public static bool IsTagAnEnemy(string tag)
    {
        for (int i = 0; i < m_EnemyTags.Length; i++)
        {
            if (tag == m_EnemyTags[i])
                return true;
        }
        return false;
    }


    void Start()
    {
        // Provide rand seed
        UnityEngine.Random.InitState(DateTime.Now.Millisecond);

        m_EnemyTags = new string[2];
        m_EnemyTags[0] = m_MeleeEnemyTag;
        m_EnemyTags[1] = m_RangerEnemyTag;
    }

    // recursive search through all children hierarchies
    public static Transform FindChildWithTag(Transform parent, string tag)
    {
        for (int i = 0; i < parent.childCount; i++)
        {
            Transform child = parent.GetChild(i);
            if (child.tag == tag)
            {
                return child;
            }
            if (child.childCount > 0)
            {
                FindChildWithTag(child, tag);
            }
        }

        return null;
    }
}

