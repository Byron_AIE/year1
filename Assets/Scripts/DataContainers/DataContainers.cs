﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DataContainers
{
    public class Double<T, J>
    {
        public Double(T Item1, J Item2)
        {
            this.Item1 = Item1;
            this.Item2 = Item2;
        }

        public T Item1 { get; set; }
        public J Item2 { get; set; }
    }
}
