﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Random = UnityEngine.Random;

public class HUDManager : MonoBehaviour
{
    [SerializeField] Image bowIcon;
    [SerializeField] Image swordIcon;

    [SerializeField] GameObject swordReticle;
    [SerializeField] GameObject bowReticle;
    [SerializeField] GameObject meleeAbilitiesGO;
    [SerializeField] GameObject rangedAbilitiesGO;

    [SerializeField] Image[] BloodLeft;
    [SerializeField] Image[] BloodRight;
    [SerializeField] Image[] BloodMid;

    [Range(0f, .4f)] [SerializeField] float FadeInTimeMin;
    [Range(.5f, 1f)] [SerializeField] float FadeInTimeMax;

    [Range(0f, .4f)] [SerializeField] float FadeOutTimeMin;
    [Range(.5f, 1f)] [SerializeField] float FadeOutTimeMax;

    [Range(.1f, .5f)] [SerializeField] float StayTimeMin;
    [Range(.5f, 3f)] [SerializeField] float StayTimeMax;

    private List<Image[]> bloodList;


    [SerializeField] float BloodAlpha = .8f;

    void Start()
    {
        ActivateWeaponHud(Weapons.WeaponTypes.Sword);
        Debug.Log("SwordHUDActive");

        bloodList = new List<Image[]>(3);

        bloodList.Add(BloodLeft);
        bloodList.Add(BloodMid);
        bloodList.Add(BloodRight);

        foreach (Image[] images in bloodList)
        {
            if (images != null || images.Length == 0)
            {
                for (int i = 0; i < images.Length; i++)
                {
                    if (!images[i])
                    {
                        Debug.LogError("Array: " + images + " contains an empty image reference at index: " + i);
                    }
                }
            }
            else
                Debug.LogError("Array: " + images + " is null or unfilled");
           
        }

        RemoveBlood();
    }

    public void ActivateWeaponHud(Weapons.WeaponTypes weaponType)
    {
        switch (weaponType)
        {
            case Weapons.WeaponTypes.Sword:
                {
                    swordIcon.CrossFadeAlpha(1f, 0.1f, false);
                    bowIcon.CrossFadeAlpha(0.2f, 0.1f, false);
                    bowReticle.SetActive(false);
                    swordReticle.SetActive(true);
                    meleeAbilitiesGO.SetActive(true);
                    rangedAbilitiesGO.SetActive(false);
                    return;
                }
            case Weapons.WeaponTypes.Bow:
                {
                    swordIcon.CrossFadeAlpha(0.2f, 0.1f, false);
                    bowIcon.CrossFadeAlpha(1f, 0.1f, false);
                    bowReticle.SetActive(true);
                    swordReticle.SetActive(false);
                    meleeAbilitiesGO.SetActive(false);
                    rangedAbilitiesGO.SetActive(true);
                    return;
                }
        }
    }    

    public void BloodSplatter()
    {       
        foreach (Image[] imageArray in bloodList)
        {
            float FadeInTime = Random.Range(FadeInTimeMin, FadeInTimeMax);
            float StayTime = Random.Range(StayTimeMin, StayTimeMax);
            float FadeOutTime = Random.Range(FadeOutTimeMin, FadeOutTimeMax);
            int n = Random.Range(1, imageArray.Length);
            Image bloodStain = imageArray[n];
            bloodStain.CrossFadeAlpha(BloodAlpha, FadeInTime, false);
            StartCoroutine(FadeBloodImage(bloodStain, 0f, (FadeInTime + StayTime), FadeOutTime));

            imageArray[n] = imageArray[0];
            imageArray[0] = bloodStain;
        }      
    }

    public void RemoveBlood()
    {
        if (bloodList == null || bloodList.Count == 0)
        {
            Debug.LogError("bloodList is empty or null");
            return;
        }

        foreach (Image[] imageArray in bloodList)
        {
            foreach(Image image in imageArray)
            {
                image.CrossFadeAlpha(0f, 0f, true);
            }
        }
    }
    
    private IEnumerator FadeBloodImage(Image image, float opacity, float stayTime, float fadeTime)
    {
        if (image)
        {
            yield return new WaitForSeconds(stayTime);
            image.CrossFadeAlpha(opacity, fadeTime, false);
        }
    }

    //public void BloodSpatterLeft()
    //{
    //    float fadeInTime = Random.Range(0f, 0.5f);
    //    float stayTime = Random.Range(2f, 5f);
    //    int n = Random.Range(1, bloodLeft.Length);
    //    bloodStainLeft = bloodLeft[n];
    //    bloodStainLeft.CrossFadeAlpha(bloodAlpha, fadeInTime, false);
    //    //Invoke("BloodFadeOutLeft", (fadeInTime + stayTime));

    //    bloodLeft[n] = bloodLeft[0];
    //    bloodLeft[0] = bloodStainLeft;
    //}

    //public void BloodSpatterRight()
    //{
    //    float fadeInTime = Random.Range(0f, 0.5f);
    //    float stayTime = Random.Range(2f, 5f);
    //    int n = Random.Range(1, bloodRight.Length);
    //    bloodStainRight = bloodRight[n];
    //    bloodStainRight.CrossFadeAlpha(bloodAlpha, fadeInTime, false);
    //    //Invoke("BloodFadeOutRight", (fadeInTime + stayTime));

    //    bloodRight[n] = bloodRight[0];
    //    bloodRight[0] = bloodStainRight;
    //}

    //public void BloodSpatterMid()
    //{
    //    float fadeInTime = Random.Range(0f, 0.5f);
    //    float stayTime = Random.Range(2f, 5f);
    //    int n = Random.Range(1, bloodMid.Length);
    //    bloodStainMid = bloodMid[n];
    //    bloodStainMid.CrossFadeAlpha(bloodAlpha, fadeInTime, false);
    //    //Invoke("BloodFadeOutMid", (fadeInTime + stayTime));

    //    bloodMid[n] = bloodMid[0];
    //    bloodMid[0] = bloodStainMid;
    //}

    //public void BloodFadeOutLeft()
    //{
    //    float fadeOutTime = Random.Range(1f, 3f);
    //    bloodStainLeft.CrossFadeAlpha(0f, fadeOutTime, false);
    //}

    //public void BloodFadeOutRight()
    //{
    //    float fadeOutTime = Random.Range(1f, 3f);
    //    bloodStainRight.CrossFadeAlpha(0f, fadeOutTime, false);
    //}

    //public void BloodFadeOutMid()
    //{
    //    float fadeOutTime = Random.Range(1f, 3f);
    //    bloodStainMid.CrossFadeAlpha(0f, fadeOutTime, false);
    //}

    //public void RemoveBlood()
    //{
    //    foreach (Image bloodStainLeft in bloodLeft)
    //    {
    //        bloodStainLeft.CrossFadeAlpha(0f, 0f, true);
    //    }

    //    foreach (Image bloodStainRight in bloodRight)
    //    {
    //        bloodStainRight.CrossFadeAlpha(0f, 0f, true);
    //    }

    //    foreach (Image bloodStainMid in bloodMid)
    //    {
    //        bloodStainMid.CrossFadeAlpha(0f, 0f, true);
    //    }
    //}

    //public void FadeOutAllBlood()
    //{
    //    foreach (Image bloodStainLeft in bloodLeft)
    //    {
    //        float fadeOutTime = Random.Range(1f, 3f);
    //        bloodStainLeft.CrossFadeAlpha(0f, fadeOutTime, false);
    //    }

    //    foreach (Image bloodStainRight in bloodRight)
    //    {
    //        float fadeOutTime = Random.Range(1f, 3f);
    //        bloodStainRight.CrossFadeAlpha(0f, fadeOutTime, false);
    //    }

    //    foreach (Image bloodStainMid in bloodMid)
    //    {
    //        float fadeOutTime = Random.Range(1f, 3f);
    //        bloodStainMid.CrossFadeAlpha(0f, fadeOutTime, false);
    //    }
    //}

    public void FadeAllBlood()
    {
        Debug.Log("FadeAllBlood");
        Invoke("FadeOutAllBlood", 5f);
    }
}
