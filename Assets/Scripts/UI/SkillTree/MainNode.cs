﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

[System.Serializable]
public class MainNode : MainTreeNodesBase, IEventSystemHandler, IPointerEnterHandler, IPointerExitHandler
{ 
    public override void Setup(NodeSetupData nodeData, TMP_Text tooltipDisplay)
    {
        base.Setup(nodeData, tooltipDisplay);              
    }

    public override void UnlockNode()
    {
        // change ui to show that button is active
        IsUnlocked = true;
    }

    public override void Reset()
    {
        base.Reset();
        SelectedNode.ResetNode();
        SelectedNode = null;
        buttonText.text = NodeText;
    }

    public override void OnChildClick(ChildNode child)
    {
        bool WasUpgradeOverriden = false;

        // if node already selected, reverse its changes before assigning new node
        if (SelectedNode != null)
        {
            WasUpgradeOverriden = true;
            SelectedNode.ResetNode();
            SelectedNode.IsSelected = false;
        }

        // if there was an override or there is no selected node
        if (WasUpgradeOverriden || SelectedNode == null)
            child.Upgrade();

        SelectedNode = child;

        buttonText.text = SelectedNode.NodeText;
        SelectedNode.IsSelected = true;

        //NextNode.IsUnlocked = true;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (IsUnlocked)
        {
            UIRectTransform.SetAsFirstSibling();
            UIElement.SetActive(true);
            tooltipTextBox.text = tooltipText;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (IsUnlocked)
        {
            UIElement.SetActive(false);
            tooltipTextBox.text = "";
        }
    }
}