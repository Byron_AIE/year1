﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

[System.Serializable]
public class ChildNode : BaseChildNode, INode, IEventSystemHandler, IPointerEnterHandler, IPointerExitHandler
{
    [HideInInspector] public string NodeText;
    [HideInInspector] public bool WasReset = false;
    [HideInInspector] public UpgradeData Data;

    private TMP_Text tooltipTextBox;
    private Text buttonText;
    private Button childButton;

    [HideInInspector] public MainTreeNodesBase ParentNode;

    public void Setup(MainTreeNodesBase ParentNode, ChildDataContainer nodeData, TMP_Text tooltipDisplay)
    {
        tooltipTextBox = tooltipDisplay;

        this.ParentNode = ParentNode;
        NodeText = nodeData.DisplayText;
        Data = nodeData.ChildData;
        TooltipText = nodeData.TooltipText;

        childButton = GetComponent<Button>();

        buttonText = transform.Find("Text").GetComponent<Text>();
        buttonText.text = NodeText;

        childButton.onClick.AddListener(UpgradeAbility);
    }
   
    public void UpgradeAbility()
    {
        if (IsSelected)
            return;

        ParentNode.OnChildClick(this);      
    }

    public void Upgrade()
    {
        UpgradeHub.Upgrade(ref Data);
    }

    public override void ResetNode()
    {     
        if (IsSelected)
        {        
            UpgradeData reversedData = UpgradeData.Reverse(ref Data);
            UpgradeHub.Upgrade(ref reversedData);
        }
        base.ResetNode();
    }

    // keep self active when hovering over
    public void OnPointerEnter(PointerEventData eventData)
    {
        gameObject.SetActive(true);

        tooltipTextBox.text = TooltipText;
    }

    public void OnPointerExit(PointerEventData eventData)
    {                   
        //tooltipTextBox.text = ParentNode.tooltipText;        
    }
}
