﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

[System.Serializable]
public class KeyStone : MainTreeNodesBase, IEventSystemHandler, IPointerEnterHandler, IPointerExitHandler
{   
    public override void Setup(NodeSetupData nodeData, TMP_Text tooltipDisplay)
    {
        base.Setup(nodeData, tooltipDisplay); 
    }

    public override void OnChildClick(ChildNode child)
    {
        // if node already selected, reverse its changes before assigning new node
        if (SelectedNode != null)
        {
            SelectedNode.ResetNode();
            SelectedNode.IsSelected = false;
        }

        SelectedNode = child;

        buttonText.text = SelectedNode.NodeText;
        SelectedNode.IsSelected = true;     
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (IsUnlocked)
        {            
            UIRectTransform.SetAsFirstSibling();
            UIElement.SetActive(true);
            tooltipTextBox.text = tooltipText;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (IsUnlocked)
        {
            UIElement.SetActive(false);
            tooltipTextBox.text = "";
        }
    }
}
