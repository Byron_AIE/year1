﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEditor;
using TMPro;


using UnityStandardAssets.Characters.FirstPerson;


// Make new class MainNode, this will hold reference to 2 children nodes which are parented to the mainnode template through the ui element, grab reference to them both to communicate between them
// child nodes need to communicate with mainnode, reference is needed

// Main node does not contain any data besides link to children and Default text to display when not set
// Reset function is necessary for death and game reset
// Abilities and weapons will handle themselves to reset, nodes need to be reset to default states

// hover over button for amount of time will display ui element, which will contain two options, two buttons, each to take that spot and fulfill their upgrades
// ui element can be attached to button as child, this ui element class (image with 2 buttons, template) will hold 2 buttons with an Upgradedata container on each of them
// Selecting one must be linked to the main node to tell that node with the data that
// (It is selected and shows which version of its child buttons was selected, perhaps changing the button image depending on if ranged melee, or neutral upgrade)
// (And changing the text to the same as its child node, main button text can stay the same in data, but the buttons display text will be changed)
// If making possible to reverse changes, need to keep track of which child button is pressed and provide method to revert upgrades Perhaps a simple flip of inputs when passing through data, can be a method which gets called
// on the Upgradedata which flips values before passing them through, effectively reversing them

// Button hovering always shows child ui element

[System.Serializable]
public class ChildDataContainer
{
    public string DisplayText;
    public string TooltipText;
    public UpgradeData ChildData;
}

[System.Serializable]
public class NodeSetupData
{
    public string MainNodeText;
    public string MainNodeTooltipText;

    public ChildDataContainer ChildData_1;
    public ChildDataContainer ChildData_2;
}

[System.Serializable]
public abstract class MainTreeNodesBase : MonoBehaviour
{
    [HideInInspector] public bool CanSelect = false;
    [HideInInspector] public bool IsSelected = false;
    [HideInInspector] public bool IsUnlocked = false;
    [HideInInspector] public MainTreeNodesBase NextNode = null;

    [HideInInspector] public ChildNode child_1;
    [HideInInspector] public ChildNode child_2;

    protected ChildNode SelectedNode;
    protected GameObject UIElement;
    protected RectTransform UIRectTransform;

    [HideInInspector] public string NodeText;
    protected Text buttonText;

    protected TMP_Text tooltipTextBox;
    [HideInInspector] public string tooltipText;

    public virtual void Setup(NodeSetupData nodeData, TMP_Text tooltipDisplay)
    {
        tooltipTextBox = tooltipDisplay;
        tooltipText = nodeData.MainNodeTooltipText;

        try
        {
            // search for children
            UIElement = GameManager.FindChildWithTag(transform, "UIElement").gameObject;
            child_1 = UIElement.transform.Find("ChildNode_1").GetComponent<ChildNode>();
            child_2 = UIElement.transform.Find("ChildNode_2").GetComponent<ChildNode>();
            buttonText = transform.Find("Text").GetComponent<Text>();
        }
        catch (Exception e)
        {
            Debug.LogException(e, this);
            Debug.LogError("Failed to find children on nodeTemplate");
        }

        NodeText = nodeData.MainNodeText;
        buttonText.text = NodeText;

        child_1.Setup(this, nodeData.ChildData_1, tooltipDisplay);
        child_2.Setup(this, nodeData.ChildData_2, tooltipDisplay);

        UIRectTransform = UIElement.GetComponent<RectTransform>();
        UIElement.SetActive(false);
    }

    public virtual void OnChildClick(ChildNode child) { }
    public virtual void UnlockNode() { }
    public virtual void SetupSkillTreeData(MainTreeNodesBase nextNode)
    {
        if (nextNode != null)
            NextNode = nextNode;
        else
            Debug.LogError("Skilltree next node is null");

        // draw connection between the nodes
    }

    public virtual void Reset()
    {
        CanSelect = IsSelected = false;        
    }

    public void ResetNode()
    {
        CanSelect = IsSelected = false;        
        child_1.ResetNode();
        child_2.ResetNode();
    }
}


public class BaseChildNode : MonoBehaviour
{
    //[HideInInspector] public bool CanSelect = false;
    [HideInInspector] public bool IsSelected = false;

    protected string TooltipText;

    public virtual void ResetNode()
    {
        IsSelected = false;
    }
}

[System.Serializable]
public struct NodeSet
{
    // if one node is null then link with the next node / keystone in the tree
    public List<NodeSetupData> Nodes;
    // change with normal node setup data, since all the same principles apply
    public NodeSetupData KeystoneData;
}

[System.Serializable]
public struct UpgradeData
{
    public UpgradeHub.UpgradeObject Object;
    public UpgradeHub.UpgradeType UpgradeType;
    public UpgradeHub.AbilityType AbilityUpgrade;
    public Bow.ArrowTypes ArrowType;

    [Tooltip("Bool for enabling functionality of certain upgrades ie. (Enable arrowspray cooldown to be reduced during berserk)")]
    public bool BoolValue;
    [Range(0f, 100f)] public int IntValue;
    [Range(0f, 100f)] public float PercentageValue;
    public float Percentage
    {
        get
        {
            decimal temp = (decimal)Math.Round((decimal)PercentageValue / (decimal)100f, 2);
            return (float)temp;
        }
        set
        {
            PercentageValue = value;
        }
    }

    public static UpgradeData Reverse(ref UpgradeData other)
    {
        UpgradeData reversedData = new UpgradeData();
        reversedData.Object = other.Object;
        reversedData.UpgradeType = other.UpgradeType;
        reversedData.ArrowType = other.ArrowType;
        reversedData.AbilityUpgrade = other.AbilityUpgrade;

        reversedData.BoolValue = !other.BoolValue;
        reversedData.IntValue = (other.IntValue * -1);
        reversedData.PercentageValue = (other.PercentageValue * -1);

        return reversedData;        
    }
}


public class SkillTree : GenericSingleton<SkillTree>
{
    [Header("Run all possible upgrades to ensure they run without errors")]
    public bool m_DebugSkillTree = false;
    public TMP_Text m_ToolTipText;
    [Header("Almost tempted to write up a windows form to make designing easier -_-")]
    [Header("Try to get into the habit of closing unneccesary nodes for faster workflow")]
    [SerializeField] List<NodeSet> m_TreeSetupData = null;
    private List<MainTreeNodesBase> m_TreeData;

    // button will have scripts attached to it
    private Button m_nodeTemplate;
    private Button m_keyStoneTemplate;
    private Transform m_treeContainer;
    private int m_TotalNodes = 0;
    private int m_CurrentNode = 0;
    [Range(1f, 300f)] public float m_NodeHorizontalSeperation = 264f;
    [Range(1f, 300f)] public float m_NodeVerticalSeperation = 90f;
    [Range(0f, 100f)] public float m_HorizontontalPadding = 20f;


    // Start is called before the first frame update
    public void Start()
    {        
        if (m_TreeSetupData == null || m_TreeSetupData.Count == 0)
        {
            Debug.LogError("No skilltree data passed through to SkillTree - Ignore if not debugging");
            return;
        }

        if (!m_ToolTipText)        
            Debug.LogWarning("TooltipText is not supplied in skilltree");        

        try
        {
            m_treeContainer = transform.Find("SkillsBlackboard");
            m_nodeTemplate = m_treeContainer.transform.Find("nodeTemplate").GetComponent<Button>();
            m_keyStoneTemplate = m_treeContainer.transform.Find("keyStoneTemplate").GetComponent<Button>();
        }
        catch (Exception e)
        {
            Debug.LogException(e, this);
            Debug.LogError("Cannot find child objects on skilltree - did object names change?");
            return;
        }

        foreach (NodeSet nodeSet in m_TreeSetupData)
        {
            m_TotalNodes += nodeSet.Nodes.Count + 1;
        }

        m_TreeData = new List<MainTreeNodesBase>(m_TotalNodes);

        RectTransform ContainerRectTransform = m_treeContainer.GetComponent<RectTransform>();
        RectTransform NodeTemplateRectTransform = m_nodeTemplate.GetComponent<RectTransform>();

        Vector3[] parentWorldPos = new Vector3[4];
        ContainerRectTransform.GetWorldCorners(parentWorldPos);

        float ParentPos_X = parentWorldPos[2].x;
        float ParentNeg_X = parentWorldPos[1].x;

        // assign initial anchor position to assign new positions to
        Vector2 NewNodeAnchorPosition = NodeTemplateRectTransform.anchoredPosition;
       
        int CurrentNodeRowNum = 0;

        // Tree Generation
        for (int i = 0; i < m_TreeSetupData.Count; i++)
        {
            // maybe flip to make inspector work easier
            for (int j = 0; j < m_TreeSetupData[i].Nodes.Count; j++)
            {
                NodeSetupData nodeData = m_TreeSetupData[i].Nodes[j];
                
                // instantiate new button based on template
                Button newButton = Instantiate(m_nodeTemplate, m_treeContainer);
                CreateNewNode(newButton, nodeData);               
               
                MainNode newNode = newButton.GetComponent<MainNode>();
           
                newNode.Setup(nodeData, m_ToolTipText);
                m_TreeData.Add(newNode);
            }

            // generate keystone
            NodeSetupData keyStoneData = m_TreeSetupData[i].KeystoneData;

            Button newKeyStoneButton = Instantiate(m_keyStoneTemplate, m_treeContainer);
            CreateNewNode(newKeyStoneButton, keyStoneData);        

            KeyStone newKeyStone = newKeyStoneButton.GetComponent<KeyStone>();
              
            newKeyStone.Setup(keyStoneData, m_ToolTipText);
            m_TreeData.Add(newKeyStone);
        }

        void CreateNewNode(Button newButton, NodeSetupData setupData)
        {
            RectTransform buttonRectTransform = newButton.GetComponent<RectTransform>();

            // assign button position to the previous nodes position
            buttonRectTransform.anchoredPosition = NewNodeAnchorPosition;

            // Perform overlap tests and adjust position if overlapping parents bounds;
            TestNodePosition(setupData, buttonRectTransform);

            // write newNode start pos
            NewNodeAnchorPosition = buttonRectTransform.anchoredPosition;

        }

        void TestNodePosition(NodeSetupData nodeData, RectTransform buttonRectTransform)
        {
            Vector2 offsetPos = new Vector2(m_NodeHorizontalSeperation, 0f);

            Vector3[] nodeWorldPos = new Vector3[4];
            buttonRectTransform.GetWorldCorners(nodeWorldPos);

            // Get next position to test against
            float buttonPosX = nodeWorldPos[2].x + (m_NodeHorizontalSeperation / 2) + m_HorizontontalPadding;
            float buttonNegX = nodeWorldPos[1].x + (m_NodeHorizontalSeperation / 2) + m_HorizontontalPadding;

            // if new position overlaps parent bounds then move node vertically from its position rather than sideways
            if (buttonPosX > ParentPos_X || buttonNegX < ParentNeg_X)
            {
                CurrentNodeRowNum = 1;
                buttonRectTransform.anchoredPosition = (NewNodeAnchorPosition + new Vector2(0, m_NodeVerticalSeperation));
                // flip increment direction
                m_NodeHorizontalSeperation *= -1;
                m_HorizontontalPadding *= -1;
            }
            else
            {
                // Dont apply offset to first node in the tree
                if (CurrentNodeRowNum != 0)
                    buttonRectTransform.anchoredPosition += offsetPos;

                CurrentNodeRowNum++;
            }
        }

        // link nodes together
        for (int i = 0; i < m_TreeData.Count; i++)
        {
            if (i + 1 < m_TreeData.Count)
                m_TreeData[i].SetupSkillTreeData(m_TreeData[i + 1]);
        }


        // Run all upgrades to test if upgrades go through without errors
        if (m_DebugSkillTree)
        {
            foreach(MainTreeNodesBase node in m_TreeData)
            {
                node.child_1.UpgradeAbility();
                node.child_2.UpgradeAbility();
            }
        }

        // unlock the first node in the tree
        m_TreeData[m_CurrentNode].UnlockNode();

        m_TreeSetupData.Clear();
        m_TreeSetupData.TrimExcess();

        GC.Collect();       

        // destroy templates
        Destroy(m_keyStoneTemplate.gameObject);
        Destroy(m_nodeTemplate.gameObject);
    }

    public void UnlockNodes()
    {
        m_TreeData[++m_CurrentNode].UnlockNode();     
    }

    public void ResetTree()
    {
        if (m_TreeData != null)
        {
            for (int i = 0; i < m_TreeData.Count; i++)
            {
                MainTreeNodesBase node = m_TreeData[i];
                node.ResetNode();
            }
        }      
    }
}

// Dictionary of function pointers?
// UpgradeInterface will contain Apply() method which will take the data and pass it through to subscribed class

public class UpgradeHub
    {
    public delegate bool OnUpgrade(ref UpgradeData data);
    public static OnUpgrade onUpgrade;   

    public enum UpgradeObject { Null, Bow, Sword, Player, Ability }
    public enum UpgradeType { Null, DamageIncrease_p, CooldownReduce_p, ArrowSpeedIncrease_p, AttackSpeedIncrease_p, MaxDistance_p, RadiusIncrease_p, PierceIncrease_i, IncreaseArrowSprayArrows_i, Health_i }
    public enum AbilityType { Null, Berserk, GroundSlam, BatteringRam, Teleport, ArrowSpray }

    public static void Upgrade(ref UpgradeData data)
    {
        onUpgrade(ref data);    
    }
}