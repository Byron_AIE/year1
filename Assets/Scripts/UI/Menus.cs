﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.Events;
using TMPro;

public class Menus : MonoBehaviour
{
    private FirstPersonController firstPersonController;
    private WaveManager wavemanager = null;
    private WaveCountdown waveCountdown = null;
    HUDManager hUDManager;

    [SerializeField] GameObject mainMenuGO = null;
    [SerializeField] GameObject gameOverScreenGO = null;
    [SerializeField] GameObject creditsScreenGO = null;
    [SerializeField] GameObject optionsScreenGO = null;
    [SerializeField] GameObject tutorialCheckGO = null;
    [SerializeField] GameObject waveCompleteScreen = null;
    [SerializeField] GameObject unspentPtsCheckGO = null;
    [SerializeField] GameObject hudGO = null;
    [SerializeField] GameObject mainMenuAssembly = null;
    [SerializeField] GameObject controlsScreenGO;
    [SerializeField] GameObject pauseScreenGO;
    
    [SerializeField] GameObject fpsControllerGO;

    [SerializeField] AudioSource menuAS = null;

    [SerializeField] AudioClip buttonPressAC = null;
    [SerializeField] AudioClip gamestartAC = null;
    [SerializeField] AudioClip gameOverAC = null;

    [SerializeField] float waveNumber = 0f;
    [SerializeField] TMP_Text waveNumberDisplay;
    [SerializeField] TMP_Text gameOverWaveNum;

    public bool inMenu = true;
    private bool paused = false;
    public bool isSkillTreeDebugging = false;

    private Weapons weapons;

    public int unspentSkillPtCount = 0;

    void Start()
    {
        menuAS.ignoreListenerPause = true;
        AudioListenerPause();
        wavemanager = GameManager.Wavemanager;
        waveCountdown = GetComponent<WaveCountdown>();    
        firstPersonController = fpsControllerGO.GetComponent<FirstPersonController>();
        weapons = fpsControllerGO.GetComponent<Weapons>();
        hUDManager = hudGO.GetComponent<HUDManager>();

        firstPersonController.SendMessage("HideSword", SendMessageOptions.DontRequireReceiver);

        SetPlayerActive(false);
        SetCursorVisibility(true);

        if (!isSkillTreeDebugging)
        {
            mainMenuGO.SetActive(true);
            tutorialCheckGO.SetActive(false);
            optionsScreenGO.SetActive(false);
            creditsScreenGO.SetActive(false);
            gameOverScreenGO.SetActive(false);
            waveCompleteScreen.SetActive(false);
            controlsScreenGO.SetActive(false);
            pauseScreenGO.SetActive(false);
        }
        else
        {
            mainMenuGO.SetActive(false);
            waveCompleteScreen.SetActive(true);
        }


        waveNumber = 0f;
    }   

    private void Update()
    {
        if (Input.GetButtonDown("Cancel") && !inMenu && !paused)
        {
            PauseGame();
        }
    }

    public void QuitGame()
    {
#if UNITY_STANDALONE
        Application.Quit();
#elif UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#endif
        Debug.Log("Quit Game");
        ButtonPressSound();
    }

    public void StartGame() //Disable main menu elements and enable player controls.
    {
        GameStartSound();
        AudioListenerResume();

        firstPersonController.ResetPlayerStats();
        inMenu = false;
        waveNumber = 0f;

        tutorialCheckGO.SetActive(false);
        mainMenuGO.SetActive(false);
        gameOverScreenGO.SetActive(false);
        SetCursorVisibility(false);
        SetPlayerActive(true);
        firstPersonController.SendMessage("SwordSheathed", SendMessageOptions.DontRequireReceiver);
        firstPersonController.SendMessage("SwordDraw", SendMessageOptions.DontRequireReceiver);
        firstPersonController.SendMessage("ShowSword", SendMessageOptions.DontRequireReceiver);
        if (!wavemanager.LoadEnemies())
            return;

        StartCoroutine(CheckStartGameConditions());
    }

    public IEnumerator CheckStartGameConditions()
    {
        while (true)
        {
            if (wavemanager.m_AllEnemiesSpawned)
            {
                StartCoroutine(waveCountdown.StartCountdown());                
                yield break;
            }

            yield return new WaitUntil(() => wavemanager.m_AllEnemiesSpawned = true);
        }       

    }

    public void Credits()
    {
        //Play epic credits sequence.
        //Debug.Log("Credits");
        mainMenuGO.SetActive(false);
        creditsScreenGO.SetActive(true);
        ButtonPressSound();
    }

    public void MainMenu()
    {
        AudioListenerPause();
        inMenu = true;
        gameOverScreenGO.SetActive(false);
        creditsScreenGO.SetActive(false);
        optionsScreenGO.SetActive(false);
        controlsScreenGO.SetActive(false);
        pauseScreenGO.SetActive(false);
        mainMenuAssembly.SetActive(true);
        mainMenuGO.SetActive(true);
        ButtonPressSound();
    }

    public void OptionsMenu()
    {
        //Open options menu.
        mainMenuGO.SetActive(false);
        optionsScreenGO.SetActive(true);
        ButtonPressSound();
    }

    public void TutorialCheck()
    {
        //Dialogue box to play or skip tutorial.
        mainMenuAssembly.SetActive(false);
        tutorialCheckGO.SetActive(true);
        ButtonPressSound();
    }

    public void ControlsScreen()
    {
        mainMenuGO.SetActive(false);
        controlsScreenGO.SetActive(true);
        ButtonPressSound();
    }

    public void PlayTutorial()
    {
        GameStartSound();
        AudioListenerResume();

        firstPersonController.ResetPlayerStats();
        inMenu = false;
        waveNumber = 0f;

        tutorialCheckGO.SetActive(false);
        mainMenuGO.SetActive(false);
        gameOverScreenGO.SetActive(false);
        SetCursorVisibility(false);
        SetPlayerActive(true);
        wavemanager.m_PlayTutorial = true;
        firstPersonController.SendMessage("SwordSheathed", SendMessageOptions.DontRequireReceiver);
        firstPersonController.SendMessage("SwordDraw", SendMessageOptions.DontRequireReceiver);
        firstPersonController.SendMessage("ShowSword", SendMessageOptions.DontRequireReceiver);
        if (!wavemanager.LoadEnemies())
            return;

        StartCoroutine(CheckStartGameConditions());

        //Start game from beginning of tutorial.
        Debug.Log("Play Tutorial");      
    }

    private void GameStartSound()
    {
        menuAS.PlayOneShot(gamestartAC);
    }

    public void ButtonPressSound()
    {
        menuAS.PlayOneShot(buttonPressAC);
    }

    public void WaveComplete()
    {
        paused = true;
        firstPersonController.StopAllAttacks();
        AudioListenerPause();
        SetPlayerActive(false);
        SetCursorVisibility(true);
        WaveCount();
        waveCompleteScreen.SetActive(true);
        if (!wavemanager.LoadEnemies())
            return;
    }

    public void NextWave()
    {
        paused = false;
        AudioListenerResume();
        SetPlayerActive(true);
        SetCursorVisibility(false);
        waveCompleteScreen.SetActive(false);
        firstPersonController.m_PlayerHealthCurrent = firstPersonController.m_PlayerHealthMax;
        firstPersonController.HealthBarUpdate();
        StartCoroutine(CheckStartGameConditions());
    }

    public void UnspentPointsCheck()
    {
        //If the player has unspent skill points and tries to start next wave, remind them.
        if (unspentSkillPtCount > 0)
        {
            unspentPtsCheckGO.SetActive(true);
        }

        else
        {
            NextWave();
        }
    }

    public void ReturnToSkillTree()
    {
        unspentPtsCheckGO.SetActive(false);
    }

    public void GameOver()
    {
        inMenu = true;
        AudioListenerPause();
        SetPlayerActive(false);
        SetCursorVisibility(true);
        gameOverScreenGO.SetActive(true);
        gameOverWaveNum.text = waveNumber.ToString();
        menuAS.PlayOneShot(gameOverAC);
    }

    private void SetPlayerActive(bool enabled)
    {
        firstPersonController.enabled = enabled;
        weapons.enabled = enabled;
        weapons.ChangeWeaponVisibility(enabled);
        hudGO.SetActive(enabled);
        hUDManager.RemoveBlood();
    }   

    private void SetCursorVisibility(bool visible)
    {
        Cursor.visible = visible;

        if (visible)
            Cursor.lockState = CursorLockMode.None;
        else
            Cursor.lockState = CursorLockMode.Locked;
    }

    public void AudioListenerPause()
    {
        AudioListener.pause = true;
    }

    public void AudioListenerResume()
    {
        AudioListener.pause = false;
    }

    public void WaveCount()
    {
        waveNumber++;
        waveNumberDisplay.text = waveNumber.ToString();
        gameOverWaveNum.text = waveNumber.ToString();
    }

    public void PauseGame()
    {
        paused = true;
        pauseScreenGO.SetActive(true);
        AudioListenerPause();
        SetPlayerActive(false);
        SetCursorVisibility(true);
        Time.timeScale = 0;
    }

    public void ResumeGame()
    {
        Time.timeScale = 1;
        paused = false;
        pauseScreenGO.SetActive(false);
        AudioListenerResume();
        SetPlayerActive(true);
        SetCursorVisibility(false);
    }
}
