﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopUps : MonoBehaviour
{
    [SerializeField] GameObject alertGO;

    void Start()
    {
        alertGO = this.gameObject;
        alertGO.SetActive(false);
    }

    public void ShowAlert()
    {
        alertGO.SetActive(true);
    }

    public void HideAlert()
    {
        alertGO.SetActive(false);
    }
}
