﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class WaveCountdown : MonoBehaviour
{
    [SerializeField] float FadeInDuration = 1f;
    [SerializeField] float FadeOutDuration = 1f;
    [SerializeField] float NumberHoldTime = .3f;
    [SerializeField] float WaitForNextNumberTime = .4f;
    [Header("Delay time between call to start wave countdown and countdown beginning")]
    [SerializeField] int   NextWaveDelayTime = 1;

    [SerializeField] TMP_Text[] Numbers;

    [SerializeField] AudioSource countdownAS;
    [SerializeField] AudioClip countdownClip;
    [SerializeField] AudioClip wavestartClip;

    [SerializeField] float hornVolume = 1f;

    private WaveManager waveManager = null;

    void Start()
    {
        waveManager = GameManager.Wavemanager;

        foreach (TMP_Text number in Numbers)
        {
            number.CrossFadeAlpha(0f, 0f, false);
        }
    }

    //private void OnTriggerEnter(Collider other) //Placeholder for testing purposes.
    //{
    //    if (other.tag == "Player" & !countdownActive)
    //    {
    //        StartCountdown();
    //    }
    //}

    public IEnumerator StartCountdown() //Call this to trigger the entire countdown. Link to 'Next Wave' button (May need a slight delay so it's not jarring).
    {
        yield return new WaitForSeconds(NextWaveDelayTime);
        StartCoroutine(FadeNumbers());
    }

    IEnumerator FadeNumbers()
    {
        foreach(TMP_Text number in Numbers)
        {
            countdownAS.PlayOneShot(countdownClip);
            if (number == Numbers[Numbers.Length - 1])
                countdownAS.PlayOneShot(wavestartClip, hornVolume);

            number.CrossFadeAlpha(1f, FadeInDuration, false);
            yield return new WaitForSeconds(FadeInDuration + NumberHoldTime);
            number.CrossFadeAlpha(0f, FadeOutDuration, false);
            yield return new WaitForSeconds(FadeOutDuration + WaitForNextNumberTime);
        }       

        waveManager.StartWave();
        yield break;
    }
}
