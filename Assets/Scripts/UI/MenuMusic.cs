﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuMusic : MonoBehaviour
{
    [SerializeField] AudioSource menuMusicAS;
    [SerializeField] float volume = 1f;

    AudioManager audioManager;
    Menus menuScript;
    CombatMusic combatMusic;

    void Start()
    {
        audioManager = FindObjectOfType<AudioManager>();
        menuScript = GameManager.Menu;
        combatMusic = FindObjectOfType<CombatMusic>();
        menuMusicAS.ignoreListenerPause = true;
    }

    private void Update()
    {
        if (menuMusicAS.isPlaying == false && menuScript.inMenu)
        {
            PlayMusicClip("MainMenu");
        }

        else if (!menuScript.inMenu)
        {
            menuMusicAS.Stop();
        }
    }

    public void PlayMusicClip(string accessString)
    {
        menuMusicAS.PlayOneShot(audioManager.GetUniqueSound(accessString), volume);
    }
}
