﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class EnemySword : MonoBehaviour
{
    //public float m_Damage = 10f;
    private Collider m_WeaponCollider;
    private FirstPersonController m_player;
    private Enemy m_self;

    private float Damage
    {
        get
        {
            return m_self.Damage;
        }
    }

    private void Start()
    {
        m_WeaponCollider = GetComponent<Collider>();
        m_player = GameManager.Player;
        m_self = transform.root.GetComponent<Enemy>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == GameManager.m_PlayerTag)
        {
            Debug.Log("SpearHitPlayer");
            m_player.TakeDamage(Damage);
            m_WeaponCollider.enabled = false;          
        }
    }  
}
