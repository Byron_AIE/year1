﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityStandardAssets.Characters.FirstPerson;


public class EnemyBasicArrow : Arrow
{
    
    public override void Setup(ArrowData data)
    {
        base.Setup(data);    
    }

    protected virtual void FixedUpdate()
    {
        if (inFlight)
        {
            Vector3 direction = transform.position - m_prevPos;

            RaycastHit hit;
            if (Physics.Raycast(m_rb.position, m_rb.transform.forward, out hit, direction.magnitude, m_rayCastMask))
            {
                GameObject hitObj = hit.collider.gameObject;

                if (hitObj.tag == GameManager.m_PlayerTag)
                {
                    Debug.Log("Arrow hit: " + hitObj.tag + " for " + Damage + " damage");
                    hitObj.GetComponent<FirstPersonController>().TakeDamage(Damage);
                    Destroy(gameObject);                   
                }
                else
                {
                    OnCollisionStopArrow(hit.point);

                    if (m_particleEffect)
                        m_particleEffect.Stop();
                }

                Debug.Log("Arrow Hit:" + hitObj);
            }

            m_prevPos = transform.position;
        }    
    }
}
