﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[CreateAssetMenu(fileName = "ArrowData", menuName = "ArrowData")]
[System.Serializable]
public class ArrowData
{
    public Bow.ArrowTypes ArrowType;
    public Arrow Prefab;

    // bow provides base damage multiplier for all arrows
    public float BaseDamage;
    public float HeavyDamageMultiplier;

    [Range(50f, 100f)] public float ArrowSpeed;
    [HideInInspector] public float ArrowSpeedMultiplier = 1f;
    [Range(76f, 100f)] public float MaxArrowSpeed;
    [Range(50f, 75f)] public float MinArrowSpeed = 50f;
    [Range(1f, 20f)] public float MaxFlightTime = 5f;
    [Range(1f, 30f)] public float ArrowDespawnTime = 5f;
    [Header("How many entities can the arrow pierce before stopping")]
    [Range(0, 5)] public int PierceNumber = 0;

    private float DefaultArrowSpeed;
    private float DefaultBaseDamage;    

    public virtual void Setup()
    {
        DefaultArrowSpeed = ArrowSpeed;
        DefaultBaseDamage = BaseDamage;
    }

    public virtual void Reset()
    {
        BaseDamage = DefaultArrowSpeed;
        ArrowSpeed = DefaultArrowSpeed;
        ArrowSpeedMultiplier = 1f;
        PierceNumber = 0;
    }
}

//[CreateAssetMenu(fileName = "ArrowSprayData", menuName = "ArrowSprayData")]
[System.Serializable]
public class ArrowSprayData : ArrowData
{
    [Header("ArrowSpray Data")]
    [Range(1, 200)] public int ArrowsToShoot = 3;
    [Range(5, 50)] public int ShotArc = 30;

    private int DefaultArrowsToShoot;
    private int DefaultShotArc;

    public override void Setup()
    {
        base.Setup();
        DefaultArrowsToShoot = ArrowsToShoot;
        DefaultShotArc = ShotArc;
    }

    public override void Reset()
    {
        base.Reset();
        ArrowsToShoot = DefaultArrowsToShoot;
        ShotArc = DefaultShotArc;
    }
}

public class Arrow : MonoBehaviour
{   
    [Range(0, 100)] protected float baseDamage = 2f;
    [HideInInspector] public virtual float Damage
    {
        get
        {
            return (baseDamage * m_bow.BaseDamageMultiplier);
        }
        private set
        {
            baseDamage = value;
        }
    }

    [Range(1, 5)] protected float HeavyDamageMultiplier = 2f;
   
    protected Rigidbody m_rb = null;
    protected Vector3 m_colHitPoint;
    protected Vector3 m_prevPos;
    protected Bow m_bow;
    protected ParticleSystem m_particleEffect = null;

    // assign through setup
    private float arrowSpeed;

    protected float ArrowSpeed
    {
        get
        {
            return arrowSpeed * ArrowSpeedMultiplier;
        }
        set
        {
            arrowSpeed = value;
        }
    }

    protected float ArrowSpeedMultiplier = 1f;
    protected float MaxFlightTime = 5f;
    protected float ArrowDespawnTime = 5f;
    protected float MaxArrowSpeed = 100f;
    protected float MinArrowSpeed = 50f;
    
    protected bool inFlight = false;
    protected bool hasCollided = false;
    protected float despawnTimer = 0f;
    protected int pierceNumber = 0;
    protected int m_rayCastMask = 0;

    public enum ArrowUsers { Player, Enemy }
    public ArrowUsers Arrowuser;

    public virtual void Setup(ArrowData data)
    {
        try
        {
            m_rb = GetComponent<Rigidbody>();
            m_colHitPoint = new Vector3();
            m_bow = GameManager.GetPlayerWeapon<Bow>(Weapons.WeaponTypes.Bow);
        }
        catch (Exception e)
        {
            Debug.LogException(e, this);
        }

        switch (Arrowuser)
        {
            case ArrowUsers.Enemy: { m_rayCastMask = GameManager.EnemyIgnoreLayerMask; break; }
            case ArrowUsers.Player: { m_rayCastMask = GameManager.PlayerIgnoreLayerMask; break; }
        }

        Damage = data.BaseDamage;
        HeavyDamageMultiplier = data.HeavyDamageMultiplier;
        ArrowSpeed = data.ArrowSpeed;
        MaxArrowSpeed = data.MaxArrowSpeed;
        MinArrowSpeed = data.MinArrowSpeed;
        ArrowDespawnTime = data.ArrowDespawnTime;
        pierceNumber = data.PierceNumber;
    }
   
    protected void Update()
    {               
        if (transform.parent != null)
        {
            if (!transform.parent.gameObject.activeSelf)
            {
                transform.parent = null;
            }
        }      
    }  

    public virtual IEnumerator ArrowFire(ArrowData data)
    {
        inFlight = true;
        float arrowSpeed = 0;

        Setup(data);
      
        switch (Arrowuser)
        {
            case ArrowUsers.Enemy: { arrowSpeed = ArrowSpeed; break; }
            case ArrowUsers.Player:
                {                    
                    if (GameManager.Player.IsArrowSpraying)
                    {
                        arrowSpeed = ArrowSpeed;
                        break;
                    }

                    arrowSpeed = m_bow.PrevWeaponChargeLevel * ArrowSpeed / MaxArrowSpeed * MaxArrowSpeed + MinArrowSpeed; break;
                }
        } 
        
        if (arrowSpeed > MaxArrowSpeed)
            arrowSpeed = MaxArrowSpeed;

        m_rb.velocity = transform.forward * arrowSpeed;
        // intial RC nextPos
        m_prevPos = m_rb.position + (m_rb.transform.forward * 3);

        StartCoroutine(CheckForDespawn(this));

        while (inFlight)
        {
            // If moving align arrow's forward along it's velocity
            if (m_rb.velocity.sqrMagnitude > 0)
            {
                transform.forward = m_rb.velocity;
            }          

            yield return null;
        }

        yield break;
    }

    protected static IEnumerator CheckForDespawn(Arrow arrow)
    {
        while (arrow.gameObject.activeSelf)
        {
            arrow.despawnTimer += Time.deltaTime;

            // if in flight for too long despawn/destroy self
            if (arrow.despawnTimer >= arrow.MaxFlightTime)
            {
                Destroy(arrow.gameObject);
                yield break;
            }

            // if collided wait for despawntime to despawn/destroy self
            if (arrow.hasCollided)
            {
                yield return new WaitForSeconds(arrow.ArrowDespawnTime);
                Destroy(arrow.gameObject);
                yield break;
            }

            yield return null;
        }       
    }   

    protected void OnEnable()
    {
        if (m_rb)
        {
            m_rb.useGravity = true;
            m_rb.isKinematic = false;
            transform.parent = null;       
        }
    }  

    protected void OnDisable()
    {
        transform.position = Vector3.zero;        
        inFlight = hasCollided = false;
        despawnTimer = 0;
    }

    protected void OnCollisionStopArrow(Vector3 hitPoint)
    {
        m_rb.useGravity = false;
        inFlight = false;
        hasCollided = true;
        m_rb.velocity = Vector3.zero;
        transform.position = hitPoint;
    }
}