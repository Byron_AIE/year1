﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpecialArrow : Arrow
{
    private float BerserkDamageMultipler
    {
        get
        {
            return m_bow.BerserkDamageMultipler;
        }
    }

    [HideInInspector]
    public override float Damage
    {
        get
        {
            return GameManager.Player.IsBerserking ? base.Damage * BerserkDamageMultipler : base.Damage;        
        }
    }

    public override void Setup(ArrowData data)
    {
        base.Setup(data);

        m_particleEffect = transform.Find("Arrow_ParticleSystem").GetComponent<ParticleSystem>();      
    }

    public override IEnumerator ArrowFire(ArrowData data)
    {
        if (m_particleEffect)
        {
            m_particleEffect.gameObject.SetActive(true);
            m_particleEffect.Play();
        }

        return base.ArrowFire(data);
    }

    protected virtual void FixedUpdate()
    {
        if (inFlight)
        {
            Vector3 direction = transform.position - m_prevPos;

            RaycastHit hit;
            if (Physics.Raycast(m_rb.position, m_rb.transform.forward, out hit, direction.magnitude, m_rayCastMask))
            {
                GameObject hitObj = hit.collider.gameObject;

                if (GameManager.IsTagAnEnemy(hitObj.tag))
                {
                    Debug.Log("Arrow hit: " + hitObj.tag + " for " + Damage + " damage");
                    hitObj.GetComponent<Enemy>().TakeDamage(Damage);

                    if (pierceNumber <= 0)
                        Destroy(gameObject);
                    else
                        pierceNumber--;
                }
                else
                {
                    OnCollisionStopArrow(hit.point);

                    if (m_particleEffect)
                        m_particleEffect.Stop();
                }

                Debug.Log("Arrow Hit:" + hitObj);
            }

            m_prevPos = transform.position;
        }
    }
}
