﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Bow : WeaponContainer
{
    private Dictionary<ArrowTypes, ArrowData> arrowDict;
    public ArrowData[] ArrowData;
    public GameObject ActualArrowGO;

    [SerializeField] Transform ArrowSpawnPoint = null;
    [SerializeField] UnityEvent ArrowSprayEvent;

    private int m_numOfArrowTypes;    

    public enum ArrowTypes { Null, Basic, Special }

    // use to grab equipped arrow data for arrow instantiation
    private ArrowTypes m_equippedType = ArrowTypes.Basic;
   
    private ArrowData EquippedArrowData
    {
        get
        {
            if (arrowDict.ContainsKey(m_equippedType))
                return arrowDict[m_equippedType];
            else
            {
                Debug.LogError("ArrowDict does not contain arrowdata of type: " + Enum.GetName(typeof(ArrowTypes), m_equippedType));
                return null;
            }
        }
    }

    public override bool Apply(ref UpgradeData data)
    {
        if (data.Object != UpgradeHub.UpgradeObject.Bow)
            return false;

        switch (data.UpgradeType)
        {
            case UpgradeHub.UpgradeType.DamageIncrease_p:
                {
                    BaseDamageMultiplier += data.Percentage;
                    break;
                }
            case UpgradeHub.UpgradeType.AttackSpeedIncrease_p:
                {
                    AttackSpeedMultiplier += data.Percentage;
                    break;
                }
            default: Debug.LogError("Object: " + this.name + " does not include method to handle upgrade of type: " + Enum.GetName(typeof(UpgradeHub.UpgradeType), data.UpgradeType) + " - this should not be called on this object"); break;
        }

        return true;
    }

    public override void Setup()
    {
        base.Setup();
        weaponType = Weapons.WeaponTypes.Bow;

        m_numOfArrowTypes = Enum.GetNames(typeof(ArrowTypes)).Length;
        arrowDict = new Dictionary<ArrowTypes, ArrowData>(m_numOfArrowTypes);

        if (ArrowSpawnPoint == null)
        {
            Debug.LogError("ArrowSpawnPoint is not supplied");
        }

        if (ArrowData == null || ArrowData.Length == 0)
        {
            Debug.LogError("ArrowData not supplied, exiting bow setup");
            return;
        }

        for (int i = 0; i < ArrowData.Length; i++)
        {
            var arrowData = ArrowData[i];
            arrowData.Setup();            
            arrowDict.Add(arrowData.ArrowType, arrowData);
        }       
    }    

    public override void Attack()
    {
        if (!player.m_IsAttacking)
            return;

        base.Attack();

        animator.SetTrigger("BowAttack");
    }

    public override void InputCheck()
    {
        if (!player.IsTeleporting)
            base.InputCheck();

        if (!player.m_IsAttacking && !player.IsTeleporting)
        {
            if (Input.GetKeyDown(KeyBinds.Ability2))
            {
                player.StartAbility(Abilities.AbilityTypes.Teleport);
            }
        }

        if (!player.m_IsAttacking && !player.IsArrowSprayOnCooldown && Input.GetKeyDown(KeyBinds.Ability1) && !player.IsUsingAbility)
        {
            ArrowSprayEvent.Invoke();
            player.m_IsAttacking = true;
        }
    }


    //public void UpgradeArrows(ArrowTypes[] types, ArrowUpgrades arrowUpgrade, UpgradeData upgradeData)
    //{
    //    // Global upgrades
    //    switch (arrowUpgrade)
    //    {
    //        case ArrowUpgrades.DamageIncrease:
    //            { BaseDamageMultiplier += upgradeData.Percentage; break; }
    //    }

    //    // Ability arrow upgrades
    //    //if (arrowUpgrade == ArrowUpgrades.ArrowSprayShotNumberIncrease)
    //    //{
    //    //    arrowSprayData.ArrowsToShoot += upgradeData.IntValue;
    //    //    return;
    //    //}

    //    // All type upgrades
    //    if (types[0] == ArrowTypes.AllArrows)
    //    {
    //        foreach (KeyValuePair<ArrowTypes, ArrowData> arrowData in arrowDict)
    //        {
    //            switch (arrowUpgrade)
    //            {
    //                case ArrowUpgrades.PierceIncrease: { arrowData.Value.PierceNumber += upgradeData.IntValue; break; }
    //                case ArrowUpgrades.SpeedIncrease: { arrowData.Value.ArrowSpeedMultiplier += upgradeData.Percentage; break; }
    //            }
    //        }

    //        return;
    //    }
    //    else
    //    {
    //        // iterate over types dictated in upgrade array
    //        for (int i = 0; i < types.Length; i++)
    //        {
    //            var arrowData = arrowDict[types[i]];

    //            switch (arrowUpgrade)
    //            {
    //                case ArrowUpgrades.PierceIncrease: { arrowData.PierceNumber += upgradeData.IntValue; break; }
    //                case ArrowUpgrades.SpeedIncrease: { arrowData.ArrowSpeedMultiplier += upgradeData.Percentage; break; }
    //            }
    //        }
    //    }      
    //}

    // Still fires only basic arrow
    public void FireArrow()
    {
        ArrowData arrowData = EquippedArrowData;

        Arrow arrow = Instantiate(arrowData.Prefab);

        arrow.transform.position = ArrowSpawnPoint.position;
        arrow.transform.forward = ArrowSpawnPoint.forward;

        // disable arrow on bowmesh
        if (ActualArrowGO)
            ActualArrowGO.SetActive(false);

        // Aim at reticle location
        Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        Vector3 aimPoint = ray.origin + ray.direction * 30;
        arrow.transform.LookAt(aimPoint);

        StartCoroutine(arrow.ArrowFire(arrowData));
    }

    public void ReactivateArrow()
    {
        if (ActualArrowGO)
            ActualArrowGO.SetActive(true);
        else
            Debug.LogError("ActualArrow not supplied in arrowData: " + EquippedArrowData);
    }
}