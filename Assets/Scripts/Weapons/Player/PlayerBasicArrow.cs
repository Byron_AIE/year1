﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBasicArrow : Arrow
{
    private float BerserkDamageMultipler
    {
        get
        {
            return m_bow.BerserkDamageMultipler;
        }
    }

    [HideInInspector] public override float Damage
    {
        get
        {         
            float BerserkMultiplier = GameManager.Player.IsBerserking ? BerserkDamageMultipler : 1f;
            return m_bow.isHeavyAttack ? base.Damage * BerserkMultiplier * HeavyDamageMultiplier : base.Damage * BerserkMultiplier;                                                        
        }       
    }   

    protected virtual void FixedUpdate()
    {
        if (inFlight)
        {
            Vector3 direction = transform.position - m_prevPos;

            RaycastHit hit;
            if (Physics.Raycast(m_rb.position, m_rb.transform.forward, out hit, direction.magnitude, m_rayCastMask))
            {
                GameObject hitObj = hit.collider.gameObject;

                if (GameManager.IsTagAnEnemy(hitObj.tag))
                {
                    Debug.Log("Arrow hit: " + hitObj.tag + " for " + Damage + " damage");
                    hitObj.GetComponent<Enemy>().TakeDamage(Damage);

                    if (pierceNumber <= 0)
                        Destroy(gameObject);
                    else
                        pierceNumber--;
                }
                else
                {
                    OnCollisionStopArrow(hit.point);

                    if (m_particleEffect)
                        m_particleEffect.Stop();
                }

                Debug.Log("Arrow Hit:" + hitObj);
            }

            m_prevPos = transform.position;
        }
    }
}
