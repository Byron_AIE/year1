﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sword : WeaponContainer
{
    public override bool Apply(ref UpgradeData data)
    {
        if (data.Object != UpgradeHub.UpgradeObject.Sword)
            return false;

        switch (data.UpgradeType)
        {
            case UpgradeHub.UpgradeType.DamageIncrease_p:
                {
                    BaseDamageMultiplier += data.Percentage;
                    break;
                }
            case UpgradeHub.UpgradeType.AttackSpeedIncrease_p:
                {
                    AttackSpeedMultiplier += data.Percentage;
                    break;
                }
            default: Debug.LogError("Object: " + this.name + " does not include method to handle upgrade of type: " + Enum.GetName(typeof(UpgradeHub.UpgradeType), data.UpgradeType) + " - this should not be called on this object"); break;
        }

        return true;
    }

    public override void Setup()
    {
        base.Setup();
        weaponType = Weapons.WeaponTypes.Sword;
    }   

    public override void InputCheck()
    {
        base.InputCheck();

        if (!player.m_IsAttacking)
        {
            // custom inputs
            if (Input.GetKeyDown(KeyBinds.Ability1))
            {
                player.StartAbility(Abilities.AbilityTypes.GroundSlam);
            }

            if (Input.GetKeyDown(KeyBinds.Ability2))
            {
                player.StartAbility(Abilities.AbilityTypes.Charge);
            }
        }
    }

    public override void Attack()
    {
        base.Attack();                      
        animator.SetTrigger("SwordAttack");        
    }

    // Hopefully in future can move colliders directly to weapon prefabs, seperate from arm mesh
    // Thus making it possible to create new sword weapons in the future if required    
}
