﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


using UnityStandardAssets.Characters.FirstPerson;

[System.Serializable]
public class WeaponContainer : MonoBehaviour, IUpgrade
{
    protected FirstPersonController player;
    //protected Weapons weapons;
    [Header("Only for melee weapons")]
    [SerializeField] protected Collider weaponCollider = null;
    [HideInInspector] public Animator animator;
    AudioManager audioManager;

    [HideInInspector]
    public bool isHeavyAttack = false;

    [Header("Ranged weapons will use their projecticles to specify damage")]
    [Range(0, 100)] public float StartDamage = 1f;
    [HideInInspector] public float BaseDamage;
    [HideInInspector] public float baseDamageMultiplier = 1f;
    [HideInInspector] public float AttackSpeedMultiplier = 1f;

    public float Damage
    {
        get
        {
            float BerserkMultiplier = player.IsBerserking ? BerserkDamageMultipler : 1f;
            return isHeavyAttack ? (BaseDamage * BaseDamageMultiplier) * BerserkMultiplier * HeavyDamageMultiplier: (BaseDamage * BaseDamageMultiplier) * BerserkMultiplier; 
        }
        protected set
        {
            BaseDamage = value;
        }
    }

    public virtual float BaseDamageMultiplier
    {
        get
        {
            return baseDamageMultiplier;
        }
        protected set
        {
            baseDamageMultiplier = value;
        }
    }

    // used by weapons to grab weaponchargelevel when mouse was released
    [HideInInspector] public float PrevWeaponChargeLevel; 


    [Range(1, 5)] public float HeavyDamageMultiplier = 2f;
    [Header("Applies for all projectiles on ranged weapons")]
    [Range(1, 5)] public float BerserkDamageMultipler = 1f;
    private float weaponChargeLevel = 0f;
    private bool isChargingAttack = false;

    [Tooltip("Rate at which weapon charges up when mouse is held")]
    [SerializeField] float weaponChargeSpeed = 1f;

    [Range(0, 5)]
    [Tooltip("Point at which weapon is successfuly charged")]
    [SerializeField] float weaponChargeLimit = 2f;

    [Range(0, 5)]
    [SerializeField] float heavyAttackThreshold = .5f;

    [SerializeField] AudioSource chargingUpAS = null;
    [SerializeField] GameObject tensionAudioGO = null;
    [SerializeField] string accessString;
    [SerializeField] float volume = 1f;

    [SerializeField] UnityEvent QuickAttackEvent;
    [SerializeField] UnityEvent HeavyAttackEvent;
    [SerializeField] UnityEvent WeaponSwapEvent;

    [HideInInspector] public Weapons.WeaponTypes weaponType;

    public virtual void Setup()
    {
        UpgradeHub.onUpgrade += Apply;

        player = GameManager.Player;
        audioManager = GameManager.Audiomanager;
        //weapons = player.GetComponent<Weapons>();
        animator = GetComponent<Animator>();

        //if (!weapons)
        //    Debug.LogError("Cannot find weapons on player");

        if (!animator)
            Debug.LogError("Cannot find weapon animator for weapon: " + name);

        BaseDamage = StartDamage;
    }

    public virtual bool Apply(ref UpgradeData data) { return true; }

    public virtual void Attack()
    {
        player.m_IsAttacking = true;
    }
   
    public virtual void ActivateCollider()
    {
        if (weaponCollider)
            weaponCollider.enabled = true;
        else
            Debug.LogError("Activate collider called on weapon without colldier - this method should not be called on this weapon");
    }

    public virtual void DeactivateCollider()
    {
        player.m_IsAttacking = false;

        if (weaponCollider)
            weaponCollider.enabled = false;
        else
            Debug.LogError("Deactivate collider called on weapon without colldier - this method should not be called on this weapon");
    }

    public void StopAttack()
    {
        Debug.Log("StopAttacks");
        animator.WriteDefaultValues();
        animator.SetFloat("WeaponCharge", 0);
        player.m_IsAttacking = false;
        isHeavyAttack = false;
    }

    public void ResetValues()
    {
        BaseDamage = StartDamage;
        BaseDamageMultiplier = 1f;
    }

    public virtual void InputCheck()
    {      
        if (!player.m_IsAttacking)
        {            
            // Default weapon attack
            if (Input.GetKeyDown(KeyBinds.Attack))
            {
                chargingUpAS.PlayOneShot(audioManager.GetRandomSound(accessString), volume);
                isHeavyAttack = false;
                StartCoroutine(CalculateCharge());
            }          
        }       

        if (Input.GetKeyUp(KeyBinds.Attack) && isChargingAttack)
        {
            isChargingAttack = false;

            if (weaponChargeLevel >= heavyAttackThreshold)
                isHeavyAttack = true;

            Attack();
            weaponChargeLevel = 0;
            tensionAudioGO.SetActive(false);
        }
    }

    public void PlayAttackSound()
    {
        if (isHeavyAttack)
            HeavyAttackEvent.Invoke();
        else
            QuickAttackEvent.Invoke();
    }

    public void WeaponSwap()
    {
        if (WeaponSwapEvent != null)
            WeaponSwapEvent.Invoke();
    }

    public void HUDSwap()
    {
        GameManager.Hudmanager.ActivateWeaponHud(weaponType);
    }

    // last keyframe
    public void WeaponHide()
    {
        gameObject.SetActive(false);
    }

    private IEnumerator CalculateCharge()
    {        
        player.m_IsAttacking = true;
        isChargingAttack = true;

        while (Input.GetKey(KeyBinds.Attack))
        {
            weaponChargeLevel += Time.deltaTime * weaponChargeSpeed;
            PrevWeaponChargeLevel = weaponChargeLevel;
            animator.SetFloat("WeaponCharge", weaponChargeLevel);

            // Exit coroutine if charge is full
            if (weaponChargeLevel >= weaponChargeLimit)
            {
                weaponChargeLevel = PrevWeaponChargeLevel = weaponChargeLimit;
                animator.SetFloat("WeaponCharge", weaponChargeLevel);
                chargingUpAS.Stop();
                tensionAudioGO.SetActive(true);

                yield break;
            }
            
            yield return null;
        }
    }   
}

public class Weapons : MonoBehaviour
{    
    public Collider SwordCollider = null;
    private FirstPersonController player;
    private Abilities abilities;

    private HUDManager hudManager;

    [HideInInspector] public WeaponContainer CurrentWeapon;

    public enum WeaponTypes { Sword, Bow }
    public WeaponContainer[] WeaponsList;

    public bool WeaponChangeComplete = true;

    [SerializeField] GameObject bowGO;
    [SerializeField] GameObject swordGO;

    [HideInInspector]
    public int NumberOfWeapons { get; private set; }

    // Start is called before the first frame update
    public void Start()
    {
        abilities = GetComponent<Abilities>();
        NumberOfWeapons = Enum.GetNames(typeof(WeaponTypes)).Length;     
        player = GameManager.Player;
        hudManager = GameManager.Hudmanager;

        if (NumberOfWeapons != WeaponsList.Length)
        {
            Debug.LogError("Weapontype enum does not equal number of weapons in WeaponsList");
        }

        // setup weapons
        foreach (WeaponContainer weapon in WeaponsList)
        {
            if (weapon == null)
                Debug.LogError("Weapon is null " + weapon.name);

            weapon.Setup();
        }

        CurrentWeapon = WeaponsList[0];
        CurrentWeapon.DeactivateCollider();
    }

    // Update is called once per frame
    void Update()
    {
        if (WeaponChangeComplete)
        {
            CurrentWeapon.InputCheck();

            // Tab weapon swap
            if (Input.GetKeyDown(KeyBinds.ChangeWeapon))
            {
                SwapWeapons();
            }

            // Scroll weapon swap
            if (player.m_UseScrollWeapons)
            {
                float scrollInput = Input.GetAxis("Mouse ScrollWheel");

                if (scrollInput > 0f || scrollInput < 0f)
                {
                    SwapWeapons(scrollInput);
                }
            }
        }    
    }

    //public void Upgrade(UpgradeData data)
    //{
    //    //switch(data.Upgrade)
    //    //{
    //    //    case UpgradeHub.Upgrades.Increase_Player_Ranged_Damage: { bow.Upgrade(data.Upgrade, data); break; }
    //    //    case UpgradeHub.Upgrades.Increase_Player_Melee_Damage: { sword.Upgrade(data.Upgrade, data); break; }
    //    //    case UpgradeHub.Upgrades.Increase_Player_Ranged_AttackSpeed: { bow.Upgrade(data.Upgrade, data); break; }
    //    //    case UpgradeHub.Upgrades.Increase_Player_Melee_AttackSpeed: { sword.Upgrade(data.Upgrade, data); break; }
    //    //    default: Debug.LogError("Upgrade type was passed to upgrade method that does not contain functionality for that method - Upgrade type: " + Enum.GetName(typeof(UpgradeHub.Upgrades), data.Upgrade)); break;
    //    //}
    //}

    public WeaponContainer GetWeaponContainer(WeaponTypes type)
    {
        foreach(WeaponContainer weapon in WeaponsList)
        {
            if (weapon != null && weapon.weaponType == type)
                return weapon;
        }

        Debug.LogError("Cannot find requested weapon type in weaponsList");
        return null;
    }

    public void SelectWeapon(WeaponTypes selectedWeapon) 
    {        
        CurrentWeapon.WeaponSwap();

        foreach (WeaponContainer weapon in WeaponsList)
        {
            if (weapon.weaponType == selectedWeapon)
            {
                CurrentWeapon = weapon;
                break;
            }
        }
    }

    public void ChangeWeaponVisibility(bool ShowAllWeapons)
    {
        ShowAllWeapons = !ShowAllWeapons;
        foreach (WeaponContainer weapon in WeaponsList)
        {            
            if (ShowAllWeapons)
            {
                weapon.gameObject.SetActive(true);
            }
            else
            {
                if (weapon.weaponType == CurrentWeapon.weaponType)
                    weapon.gameObject.SetActive(true);
                else
                    weapon.gameObject.SetActive(false);
            }          
        }
    }

    public void SwapWeapons(float scrollValue = 0)
    {
        if (player.m_IsAttacking)
            return;

        WeaponTypes newWeapon;
        newWeapon = CurrentWeapon.weaponType;

        // If scrolling to change weapon
        if (scrollValue != 0)
        {
            if (scrollValue > 0)
            {
                if ((int)newWeapon >= NumberOfWeapons - 1)
                {
                    newWeapon = 0;
                }
                else
                {
                    newWeapon++;
                }
            }
            else if (scrollValue < 0)
            {
                if (newWeapon == 0)
                {
                    newWeapon = (WeaponTypes)NumberOfWeapons - 1;
                }
                else
                {
                    newWeapon--;
                }
            }
            else
            {
                Debug.LogError("Invalid scroll input: " + scrollValue);
            }
        }
        else
        {
            if ((int)newWeapon >= NumberOfWeapons - 1)
            {
                // reset enum state to beginning
                newWeapon = 0;
            }
            else
            {
                newWeapon++;
            }
        }

        SelectWeapon(newWeapon);        
    }

    public void ResetAllWeapons()
    {
        foreach(WeaponContainer weapon in WeaponsList)
        {
            weapon.ResetValues();
        }
    }

    // bit janky since weapons aren't seperate from arm mesh
    private void OnTriggerEnter(Collider other)
    {
        if (GameManager.IsTagAnEnemy(other.tag))
        {
            Debug.Log("Sword hit: " + other.tag + " for " + CurrentWeapon.Damage + " damage");
            other.GetComponent<Enemy>().TakeDamage(CurrentWeapon.Damage);
        }
    }

    // second to last keyframe
    public void CallHUDSwap()
    {
        CurrentWeapon.HUDSwap();
    }

    public void ActivateSwordCollider()
    {
        SwordCollider.enabled = true;
    }

    public void DisableSwordCollider()
    {
        SwordCollider.enabled = false;
        player.m_IsAttacking = false;
    }   

    public void CompleteWeaponChange()
    {
        WeaponChangeComplete = true;
    }

    public void StartWeaponChange()
    {
        WeaponChangeComplete = false;
    }

    public void ActivateBow()
    {
        bowGO.SetActive(true);
    }

    public void ActivateSword()
    {
        swordGO.SetActive(true);
    }

    public void HideBow()
    {
        bowGO.SetActive(false);
    }

    public void HideSword()
    {
        swordGO.SetActive(false);
    }
}
