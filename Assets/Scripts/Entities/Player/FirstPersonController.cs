using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Utility;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.Rendering.PostProcessing;

using Random = UnityEngine.Random;

namespace UnityStandardAssets.Characters.FirstPerson
{
    [RequireComponent(typeof(CharacterController))]
    [RequireComponent(typeof(AudioSource))]
    public class FirstPersonController : MonoBehaviour, IUpgrade
    {
        [SerializeField] private bool m_IsWalking;
        [SerializeField] private float m_WalkSpeed;
        [SerializeField] private float m_RunSpeed;
        [SerializeField] [Range(0f, 1f)] private float m_RunstepLenghten;
        [SerializeField] private float m_JumpSpeed;
        [SerializeField] private float m_StickToGroundForce;
        [SerializeField] private float m_GravityMultiplier;
        [SerializeField] private MouseLook m_MouseLook;
        [SerializeField] private bool m_UseFovKick;
        [SerializeField] private FOVKick m_FovKick = new FOVKick();
        [SerializeField] private bool m_UseHeadBob;
        [SerializeField] private CurveControlledBob m_HeadBob = new CurveControlledBob();
        [SerializeField] private LerpControlledBob m_JumpBob = new LerpControlledBob();
        [SerializeField] private float m_StepInterval;
        [SerializeField] private AudioClip[] m_FootstepSounds;
        [SerializeField] private AudioClip[] m_FootstepSoundsArmoured;    // an array of footstep sounds that will be randomly selected from.
        [SerializeField] private AudioClip m_JumpSound;           // the sound played when character leaves the ground.
        [SerializeField] private AudioClip m_LandSound;           // the sound played when character touches back on ground.

        private Camera m_Camera;
        private bool m_Jump;
        private float m_YRotation;
        private Vector2 m_Input;
        private Vector3 m_MoveDir = Vector3.zero;
        private CharacterController m_CharacterController;
        private CollisionFlags m_CollisionFlags;
        private bool m_PreviouslyGrounded;
        private Vector3 m_OriginalCameraPosition;
        private float m_StepCycle;
        private float m_NextStep;
        private bool m_Jumping;
        private AudioSource m_AudioSource;

        [SerializeField] float landingSoundVolume = 1f;
        [SerializeField] float footstepsVolume = 1f;
        [SerializeField] float footstepsVolumeArmoured = 1f;
        [SerializeField] float jumpSoundVolume = 1f;

        [SerializeField] UnityEvent deathEvent;
        [SerializeField] UnityEvent ouchEvent;
        [SerializeField] float ouchCooldown = 1f;
        [SerializeField] UnityEvent bloodEvent;
        [SerializeField] UnityEvent bloodFade;

        List<IUpgrade> upgradeList;

        public enum UpgradeType { Null, Health }

        Dictionary<UpgradeType, object> upgradeListeners;

        public float m_ControllerRadius { get { return m_CharacterController.radius; } }
        public float m_ControllerHeight { get { return m_CharacterController.height; } }
        public Vector3 m_PlayerBase
        {
            get
            {
                Vector3 playerBase = m_CharacterController.transform.position;
                playerBase.z -= (m_CharacterController.height / 2);
                return playerBase;
            }
        }
        public Vector3 m_ControllerForward { get { return m_CharacterController.transform.forward; } }

        public bool CanCollide
        {
            get
            {
                return m_CharacterController.detectCollisions;
            }
            set
            {
                m_CharacterController.detectCollisions = value;
            }
        }

        // ignores berserk
        public bool IsUsingAbility
        {
            get
            {
                return m_abilities.IsAnyAbilityBeingUsed;
            }
        }

        public bool IsArrowSpraying
        {
            get
            {
                return m_abilities.GetAbilityData<bool>(Abilities.AbilityTypes.ArrowSpray, Abilities.AbilityDataType.UsingAbility);
            }
        }

        public bool IsBerserking
        {
            get
            {
                return m_abilities.IsBerserking;
            }
        }

        public bool IsTeleporting
        {
            get
            {
                return m_abilities.IsTeleporting;
            }
        }

        public bool IsArrowSprayOnCooldown
        {
            get
            {
                return m_abilities.GetAbilityData<bool>(Abilities.AbilityTypes.ArrowSpray, Abilities.AbilityDataType.Cooldown);
            }
        }

        public bool IsChargeOnCooldown
        {
            get
            {
                return m_abilities.GetAbilityData<bool>(Abilities.AbilityTypes.Charge, Abilities.AbilityDataType.Cooldown);
            }
        }

        public bool IsGrounded
        {
            get
            {
                return m_CharacterController.isGrounded;
            }
        }    

        public bool IsWeaponAnimationPlaying
        {
            get
            {
                return m_weapons.CurrentWeapon.animator.GetCurrentAnimatorStateInfo(0).IsName("Default");
            }
        }

        public bool IsCharging
        {
            get
            {
                return m_abilities.GetAbilityData<bool>(Abilities.AbilityTypes.Charge, Abilities.AbilityDataType.UsingAbility);
            }
        }

        public WeaponContainer GetWeapon(Weapons.WeaponTypes type)
        {
            return m_weapons.GetWeaponContainer(type);
        }

        ///////////////////////////////////////////

        // Custom Player Fields

        ///////////////////////////////////////////

        [Header("Exposed for use in debug only")]
        [Range(0, 100)]
        public float m_PlayerHealthCurrent = 100;
        public float m_PlayerHealthMax = 100;
       
        public bool m_IsAttacking = false;
        public bool m_UseScrollWeapons = true;
        public bool m_PlayerAlive = true;
        [HideInInspector] public Transform m_AnchorPoint;

        [Header("Vignette take damage effect")]
        public float m_VignetteEffectSpeed = 1f;
        [Range(0.1f, 1f)] public float m_VignetteMaxOpacity = .5f;

        private bool m_canOuch = true;
        private bool m_isEffectsDamageRunning = false;
        [SerializeField] AudioClip m_DamageTakeClip;

        [SerializeField] SkillTree m_SkillTree;
        private Abilities m_abilities;
        private Weapons m_weapons;
        private Rigidbody m_Rb;
        private AudioSource m_audioPlayer;
        private Menus m_menusScript;
        private HUDManager m_hudManager;
        private PostProcessVolume m_PPVolume;
        private Vignette m_vignette;
        private Image m_HealthBarFill;

        [HideInInspector] public bool m_CanMove = true;      

        // Use this for initialization
        private void Start()
        {
            m_CharacterController = GetComponent<CharacterController>();
            m_Camera = Camera.main;
            m_OriginalCameraPosition = m_Camera.transform.localPosition;
            m_FovKick.Setup(m_Camera);
            m_HeadBob.Setup(m_Camera, m_StepInterval);
            m_StepCycle = 0f;
            m_NextStep = m_StepCycle / 2f;
            m_Jumping = false;
            m_AudioSource = GetComponent<AudioSource>();
            m_MouseLook.Init(transform, m_Camera.transform);
            m_PPVolume = GameManager.PPVolume;
            m_hudManager = GameManager.Hudmanager;
            m_AnchorPoint = GameObject.FindGameObjectWithTag("PlayerAnchor").transform;
            try
            {
                m_HealthBarFill = GameObject.FindGameObjectWithTag("HealthBar").GetComponent<Image>();
                m_menusScript = GameManager.Menu;
                m_abilities = GetComponent<Abilities>();                          
                m_weapons = GetComponent<Weapons>();
                m_audioPlayer = GetComponent<AudioSource>();              
                m_Rb = GetComponent<Rigidbody>();

                if (m_abilities)
                    m_abilities.Setup();
                else
                    Debug.LogError("Cannot find abilities component on player");
             
                if (!m_AnchorPoint)
                {
                    Debug.LogError("Anchorpoint not assigned on player");
                }
            }
            catch (Exception e)
            {
                Debug.LogException(e, this);
                Debug.LogError("Player setup incomplete - Missing components");
            }

            UpgradeHub.onUpgrade += Apply;
        }


        // Update is called once per frame
        private void Update()
        {
            RotateView();
            if (!IsCharging || m_CanMove)
            {
                // the jump state needs to read here to make sure it is not missed
                if (!m_Jump)
                {
                    m_Jump = Input.GetKeyDown(KeyBinds.Jump);
                }

                if (!m_PreviouslyGrounded && m_CharacterController.isGrounded)
                {
                    StartCoroutine(m_JumpBob.DoBobCycle());
                    PlayLandingSound();
                    m_MoveDir.y = 0f;
                    m_Jumping = false;
                }
                if (!m_CharacterController.isGrounded && !m_Jumping && m_PreviouslyGrounded)
                {
                    m_MoveDir.y = 0f;
                }

                m_PreviouslyGrounded = m_CharacterController.isGrounded;
            }            
            
            // berserk
            //if (Input.GetKeyDown(KeyBinds.Ult))
            //{
            //    StartAbility(Abilities.AbilityTypes.Berserk);
            //}          
        }   

        private void PlayLandingSound()
        {
            int n = Random.Range(1, m_FootstepSoundsArmoured.Length);
            m_AudioSource.clip = m_FootstepSoundsArmoured[n];
            m_AudioSource.PlayOneShot(m_AudioSource.clip, landingSoundVolume);
            // move picked sound to index 0 so it's not picked next time
            m_FootstepSoundsArmoured[n] = m_FootstepSoundsArmoured[0];
            m_FootstepSoundsArmoured[0] = m_AudioSource.clip;
            m_NextStep = m_StepCycle + .5f;
        }


        private void FixedUpdate()
        {           
            if (m_CanMove)
            {
                float speed;
                GetInput(out speed);
                // always move along the camera forward as it is the direction that it being aimed at
                Vector3 desiredMove = transform.forward * m_Input.y + transform.right * m_Input.x;

                // get a normal for the surface that is being touched to move along it
                RaycastHit hitInfo;
                Physics.SphereCast(transform.position, m_CharacterController.radius, Vector3.down, out hitInfo,
                                    m_CharacterController.height / 2f, Physics.AllLayers, QueryTriggerInteraction.Ignore);
                desiredMove = Vector3.ProjectOnPlane(desiredMove, hitInfo.normal).normalized;

                m_MoveDir.x = desiredMove.x * speed;
                m_MoveDir.z = desiredMove.z * speed;


                if (m_CharacterController.isGrounded)
                {
                    m_MoveDir.y = -m_StickToGroundForce;
                    if (!IsCharging)
                    {
                        if (m_Jump)
                        {
                            m_MoveDir.y = m_JumpSpeed;
                            PlayJumpSound();
                            m_Jump = false;
                            m_Jumping = true;
                        }
                    }
                }
                else
                {
                    m_MoveDir += Physics.gravity * m_GravityMultiplier * Time.fixedDeltaTime;
                }

                m_CollisionFlags = m_CharacterController.Move(m_MoveDir * Time.fixedDeltaTime);

                if (!IsCharging)
                {
                    ProgressStepCycle(speed);
                }

                UpdateCameraPosition(speed);

                m_MouseLook.UpdateCursorLock();
            }           
        }


        private void PlayJumpSound()
        {
            int n = Random.Range(1, m_FootstepSoundsArmoured.Length);
            m_AudioSource.clip = m_FootstepSoundsArmoured[n];
            m_AudioSource.PlayOneShot(m_AudioSource.clip, jumpSoundVolume);
            // move picked sound to index 0 so it's not picked next time
            m_FootstepSoundsArmoured[n] = m_FootstepSoundsArmoured[0];
            m_FootstepSoundsArmoured[0] = m_AudioSource.clip;
        }


        private void ProgressStepCycle(float speed)
        {
            if (m_CharacterController.velocity.sqrMagnitude > 0 && (m_Input.x != 0 || m_Input.y != 0))
            {
                m_StepCycle += (m_CharacterController.velocity.magnitude + (speed * (m_IsWalking ? 1f : m_RunstepLenghten))) *
                             Time.fixedDeltaTime;
            }

            if (!(m_StepCycle > m_NextStep))
            {
                return;
            }

            m_NextStep = m_StepCycle + m_StepInterval;

            PlayFootStepAudio();
            PlayFootStepAudioArmoured();
        }


        private void PlayFootStepAudio()
        {
            if (!m_CharacterController.isGrounded)
            {
                return;
            }
            // pick & play a random footstep sound from the array,
            // excluding sound at index 0
            int n = Random.Range(1, m_FootstepSounds.Length);
            m_AudioSource.clip = m_FootstepSounds[n];
            m_AudioSource.PlayOneShot(m_AudioSource.clip, footstepsVolume);
            // move picked sound to index 0 so it's not picked next time
            m_FootstepSounds[n] = m_FootstepSounds[0];
            m_FootstepSounds[0] = m_AudioSource.clip;
        }

        private void PlayFootStepAudioArmoured()
        {
            if (!m_CharacterController.isGrounded)
            {
                return;
            }
            // pick & play a random footstep sound from the array,
            // excluding sound at index 0
            int n = Random.Range(1, m_FootstepSoundsArmoured.Length);
            m_AudioSource.clip = m_FootstepSoundsArmoured[n];
            m_AudioSource.PlayOneShot(m_AudioSource.clip, footstepsVolumeArmoured);
            // move picked sound to index 0 so it's not picked next time
            m_FootstepSoundsArmoured[n] = m_FootstepSoundsArmoured[0];
            m_FootstepSoundsArmoured[0] = m_AudioSource.clip;
        }


        private void UpdateCameraPosition(float speed)
        {
            Vector3 newCameraPosition;
            if (!m_UseHeadBob)
            {
                return;
            }
            if (m_CharacterController.velocity.magnitude > 0 && m_CharacterController.isGrounded)
            {
                m_Camera.transform.localPosition =
                    m_HeadBob.DoHeadBob(m_CharacterController.velocity.magnitude +
                                      (speed * (m_IsWalking ? 1f : m_RunstepLenghten)));
                newCameraPosition = m_Camera.transform.localPosition;
                newCameraPosition.y = m_Camera.transform.localPosition.y - m_JumpBob.Offset();
            }
            else
            {
                newCameraPosition = m_Camera.transform.localPosition;
                newCameraPosition.y = m_OriginalCameraPosition.y - m_JumpBob.Offset();
            }
            m_Camera.transform.localPosition = newCameraPosition;
        }


        private void GetInput(out float speed)
        {
            // Read input
            float horizontal = 0f;
            if (Input.GetKey(KeyBinds.Right) && !Input.GetKey(KeyBinds.Left))
                horizontal = 1f;
            else if (Input.GetKey(KeyBinds.Left) && !Input.GetKey(KeyBinds.Right))
                horizontal = -1f;

            float vertical = 0f;
            if (!IsCharging && Input.GetKey(KeyBinds.Forward) && !Input.GetKey(KeyBinds.Back))
                vertical = 1f; 
            else if (!IsCharging && Input.GetKey(KeyBinds.Back) && !Input.GetKey(KeyBinds.Forward))
                vertical = -1f;

            bool waswalking = m_IsWalking;

#if !MOBILE_INPUT
            // On standalone builds, walk/run speed is modified by a key press.
            // keep track of whether or not the character is walking or running
             m_IsWalking = !Input.GetKey(KeyBinds.Mobility);
#endif
            // set the desired speed to be walking or running
            speed = m_IsWalking ? m_WalkSpeed : m_RunSpeed;
            m_Input = new Vector2(horizontal, vertical);

            // normalize input if it exceeds 1 in combined length:
            if (m_Input.sqrMagnitude > 1)
            {
                m_Input.Normalize();
            }

            // handle speed change to give an fov kick
            // only if the player is going to a run, is running and the fovkick is to be used
            if (m_IsWalking != waswalking && m_UseFovKick && m_CharacterController.velocity.sqrMagnitude > 0)
            {

                // uncomment with fix

                if (!IsCharging && !IsChargeOnCooldown && !IsTeleporting)
                {
                    StopAllCoroutines();
                }
                StartCoroutine(!m_IsWalking ? m_FovKick.FOVKickUp() : m_FovKick.FOVKickDown());
            }
        }


        private void RotateView()
        {
            m_MouseLook.LookRotation(transform, m_Camera.transform);
        }


        private void OnControllerColliderHit(ControllerColliderHit hit)
        {
            Rigidbody body = hit.collider.attachedRigidbody;
            //dont move the rigidbody if the character is on top of it
            if (m_CollisionFlags == CollisionFlags.Below)
            {
                return;
            }

            if (body == null || body.isKinematic)
            {
                return;
            }
            body.AddForceAtPosition(m_CharacterController.velocity * 0.1f, hit.point, ForceMode.Impulse);
        }

        public bool StartAbility(Abilities.AbilityTypes type)
        {
            if (!IsUsingAbility)
                m_abilities.StartAbility(type);

            return true;
        }      

        public void ResetPlayerStats()
        {
            m_PlayerAlive = true;
            m_PlayerHealthCurrent = m_PlayerHealthMax;
            HealthBarUpdate();
            m_abilities.ResetAllAbilities();
            m_weapons.ResetAllWeapons();
        }
               
        public void TakeDamage(float damage)
        {
            if (damage >= 0)
            {
                m_PlayerHealthCurrent -= damage;
                HealthBarUpdate();
                m_hudManager.BloodSplatter();
                StartCoroutine(TakeDamageEffects());
                if (m_canOuch)
                {
                    m_canOuch = false;
                    ouchEvent.Invoke();
                    Invoke("ResetOuch", ouchCooldown);
                }
                if (m_PlayerHealthCurrent <= 0 && m_PlayerAlive)
                {
                    m_PlayerHealthCurrent = 0;
                    m_PlayerAlive = false;
                    Death();
                    // Player is dead
                }
            }
            else
                Debug.LogError("Damage was negative for: " + this.name);
        }

        public IEnumerator TakeDamageEffects()
        {
            if (m_isEffectsDamageRunning)
                yield break;

            m_isEffectsDamageRunning = true;
            m_PPVolume.profile.TryGetSettings(out m_vignette);

            float vignetteOpacity = 0.1f;           

            while (vignetteOpacity > 0.09f)
            {
                vignetteOpacity = Mathf.PingPong(Time.time * m_VignetteEffectSpeed, m_VignetteMaxOpacity);

                m_vignette.intensity.value = vignetteOpacity;
                yield return null;
            }

            m_vignette.intensity.value = 0f;
            m_isEffectsDamageRunning = false;

        }

        public bool Apply(ref UpgradeData data)
        {
            Debug.Log("Hit apply");

            if (data.Object != UpgradeHub.UpgradeObject.Player)
                return false;

            switch(data.UpgradeType)
            {
                case UpgradeHub.UpgradeType.Health_i:
                    {
                        m_PlayerHealthCurrent += data.Percentage;
                        break;
                    }
                default: Debug.LogError("Object: " + this.name + " does not include method to handle upgrade of type: " + Enum.GetName(typeof(UpgradeHub.UpgradeType), data.UpgradeType) + " - this should not be called on this object"); break;

            }
            return true;
        }

        //public void Upgrade(UpgradeHub.Upgrades upgrade, UpgradeData data)
        //{
            //switch (upgrade)
            //{
            //    case UpgradeHub.Upgrades.Increase_Player_Health: { m_PlayerHealthCurrent += data.Percentage; break; }
            //    case UpgradeHub.Upgrades.Increase_Player_Ranged_Damage: { m_weapons.bow.Upgrade(upgrade, data); break; }
            //    case UpgradeHub.Upgrades.Increase_Player_Ranged_AttackSpeed: { m_weapons.bow.Upgrade(upgrade, data); break; }
            //    case UpgradeHub.Upgrades.Increase_Player_Melee_Damage:
            //        {
            //            m_weapons.sword.Upgrade(upgrade, data);
            //            m_abilities.groundSlamAbility.BaseDamageMultiplier += data.Percentage;
            //            m_abilities.chargeAbility.BaseDamageMultiplier += data.Percentage;
            //            break;
            //        }
            //    case UpgradeHub.Upgrades.Increase_Player_Melee_AttackSpeed: { m_weapons.sword.Upgrade(upgrade, data); break; }

            //    // abilities
            //    case UpgradeHub.Upgrades.Decrease_BatteringRam_Cooldown: { m_abilities.chargeAbility.ReduceCooldown(data); break; }
            //    case UpgradeHub.Upgrades.Decrease_Berserk_Cooldown: { m_abilities.berserkAbility.ReduceCooldown(data); break; }
            //    case UpgradeHub.Upgrades.Decrease_Groundslam_Cooldown: { m_abilities.groundSlamAbility.ReduceCooldown(data); break; }
            //    case UpgradeHub.Upgrades.Decrease_Teleport_Cooldown: { m_abilities.teleportAbility.ReduceCooldown(data); break; }
            //    case UpgradeHub.Upgrades.Decrease_Arrowspray_Cooldown: { m_abilities.arrowSprayAbility.ReduceCooldown(data); break; }
            //    case UpgradeHub.Upgrades.Berserk_Arrowspray_Cooldown_Decrease: { m_abilities.arrowSprayAbility.UseBerserkReducedCooldown = data.UpgradeAbility; break; }
            //    case UpgradeHub.Upgrades.BatteringRam_Activate_Frenzy_Capability: { m_abilities.chargeAbility.UseFrenzy = data.UpgradeAbility; break; }
            //    case UpgradeHub.Upgrades.Increase_Player_Teleport_Distance: { m_abilities.teleportAbility.MaxTeleportDistance += data.Percentage; break; }

            //        //case UpgradeHub.Upgrades.Increase_Player_Arrowpray_Shots { weapons.bow; break; }
            //}
        //}

        public void StopAllAttacks()
        {
            //Debug.Log("StopAttacksPlayer");
            m_weapons.CurrentWeapon.StopAttack();

            m_PPVolume.profile.TryGetSettings(out m_vignette);
            m_vignette.intensity.value = 0f;
        }

        public void MovePosition(Vector3 pos)
        {
            m_CharacterController.enabled = false;
            m_CharacterController.transform.position = (pos + new Vector3 (0, m_CharacterController.height / 2, 0));
            m_CharacterController.enabled = true;
        }

        public void MoveController(Vector3 motion)
        {            
            m_CharacterController.Move(motion);
        }

        public void SimpleMoveController(Vector3 speed)
        {
            Vector3 newMotion = speed + new Vector3(0, m_CharacterController.height / 2, 0);
            m_CharacterController.SimpleMove(newMotion);
        }

        public void MoveTransform(Vector3 newPos)
        {
            m_CharacterController.enabled = false;
            m_CharacterController.transform.position = newPos + new Vector3(0, m_CharacterController.height / 2, 0);
            m_CharacterController.enabled = true;
        }

        public void HealthBarUpdate()
        {
            m_HealthBarFill.fillAmount = m_PlayerHealthCurrent / 100f;
        }

        private void Death()
        {
            m_canOuch = true;
            m_audioPlayer.Stop();
            deathEvent.Invoke();
            StopAllAttacks();
            m_menusScript.GameOver();
            GameManager.Wavemanager.RestartWaves();

            if (m_SkillTree)
                m_SkillTree.ResetTree();
        }

        private void ResetOuch()
        {
            m_canOuch = true;
        }
    }
}
