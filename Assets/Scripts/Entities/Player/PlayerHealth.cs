﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public float startingHealth = 100f;
    [SerializeField]
    private float health;
    // Start is called before the first frame update
    void Start()
    {
        health = startingHealth;
    }

    public void TakeDamage(float damage)
    {
        if (damage <= 0f)
        {
            Debug.Log("Player took negative damage");
            return;
        }
        health -= damage;
        if (health <= 0f)
        {
            Death();
        }
    }

    private void Death()
    {
        //The player is dead
    }
}
