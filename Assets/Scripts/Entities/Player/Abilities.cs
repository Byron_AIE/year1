﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

using UnityStandardAssets.Characters.FirstPerson;

// if making transition from SO then make apply baseCooldown to every 

[System.Serializable]
public abstract class AbilityData : IUpgrade
{
    // make as a default
    [Range(0.1f, 30f)] [SerializeField] float cooldownTime = .1f;
    // make on every class
    [HideInInspector] public float CooldownMultiplier = 1f;

    // keep on SO
    public Image CooldownImage = null;
    public UnityEvent OnCooldownFinishEvent;

    public virtual float CooldownTime
    {
        get
        {
            return cooldownTime * CooldownMultiplier;
        }
        set
        {
            cooldownTime = value;
        }
    }

    protected float DefaultCooldownTime;

    [HideInInspector] public bool UsingAbility = false;
    [HideInInspector] public bool OnCooldown = false;    
    [HideInInspector] public bool IsUnlocked = false;

    public virtual bool Apply(ref UpgradeData data)
    {       
        if (data.Object != UpgradeHub.UpgradeObject.Ability)
            return false;

        return true;
    }

    public virtual bool StartAbility()
    {
        if (UsingAbility || OnCooldown || this == null)
            return false;

        return true;
    }

    public virtual void Reset()
    {
        CooldownTime = DefaultCooldownTime;
        CooldownMultiplier = 1f;
    }

    public virtual void Setup()
    {

        
        DefaultCooldownTime = cooldownTime;
    }

    public void ReduceCooldown(UpgradeData data)
    {
        if (data.Percentage < 0)
            CooldownMultiplier -= data.Percentage;
        else
            CooldownMultiplier += data.Percentage;
    }
}
[CreateAssetMenu(fileName = "ChargeAbility", menuName = "ChargeAbility")]
[System.Serializable]
public class ChargeAbility : AbilityData
{
    public ChargeAbility(ChargeAbility abilityData)
    {
        if (abilityData != null)
        {
            this.BaseDamage = abilityData.BaseDamage;
            this.BerserkDamageMultipler = abilityData.BerserkDamageMultipler;           
            this.CooldownTime = abilityData.CooldownTime;
            this.ChargeThrust = abilityData.ChargeThrust;
            this.ChargeTime = abilityData.ChargeTime;
        }
    }

    [Header("Custom values")]
    [Range(1f, 100f)] public float BaseDamage = 1f;
    [HideInInspector] [Range(1f, 100f)] private float BaseDamageMultiplier = 1f;
    [Range(1f, 5f)] public float BerserkDamageMultipler = 1f;

    public float ChargeThrust;
    public float ChargeTime;
    public Collider ChargeCollider;
    [HideInInspector] public bool UseFrenzy = false;
    [Header("Tags for objects in the environment that can be hit during a charge")]
    public string[] ChargeHitTags;

    [HideInInspector]
    public float Damage
    {
        get
        {
            return GameManager.Player.IsBerserking ? (BaseDamage * BaseDamageMultiplier) * BerserkDamageMultipler : (BaseDamage * BaseDamageMultiplier);
        }
        private set
        {
            BaseDamage = value;
        }
    }

    private float DefaultDamage;
    private float DefaultBerserkDamageMultipler;
    private float DefaultChargeThrust;
    private float DefaultChargeTime;

    public override bool Apply(ref UpgradeData data)
    {
        if (!base.Apply(ref data) ||
            data.AbilityUpgrade != UpgradeHub.AbilityType.BatteringRam)
            return false;

        switch(data.UpgradeType)
        {
            case UpgradeHub.UpgradeType.CooldownReduce_p:
                {
                    CooldownTime -= data.Percentage;
                    break;
                }
            case UpgradeHub.UpgradeType.MaxDistance_p:
                {
                    ChargeTime += data.Percentage;
                    break;
                }
            default: Debug.LogError("Object: " + this + " does not include method to handle upgrade of type: " + Enum.GetName(typeof(UpgradeHub.UpgradeType), data.UpgradeType) + " - this should not be called on this object"); break;
        }

        return true;
    }

    public override void Reset()
    {
        base.Reset();
        BaseDamage = DefaultDamage;
        BerserkDamageMultipler = DefaultBerserkDamageMultipler;
        ChargeThrust = DefaultChargeThrust;
        ChargeTime = DefaultChargeTime;
        UseFrenzy = false;
    }

    public override void Setup()
    {
        base.Setup();

        DefaultDamage = BaseDamage;
        DefaultBerserkDamageMultipler = BerserkDamageMultipler;
        DefaultChargeThrust = ChargeThrust;
        DefaultChargeTime = ChargeTime;

        ChargeCollider.enabled = false;
    }
}

[CreateAssetMenu(fileName = "GroundSlamAbility", menuName = "GroundSlamAbility")]
[System.Serializable]
public class GroundSlamAbility : AbilityData
{
    public GroundSlamAbility(GroundSlamAbility abilityData)
    {
        if (abilityData != null)
        {
            this.BaseDamage = abilityData.BaseDamage;
            this.BerserkDamageMultipler = abilityData.BerserkDamageMultipler;
            this.BlastForce = abilityData.BlastForce;
            this.CooldownTime = abilityData.CooldownTime;
            this.GroundSlamRadius = abilityData.GroundSlamRadius;
        }
    }

    [Range(1f, 100f)] public float BaseDamage = 1f;
    [HideInInspector] [Range(1f, 100f)] private float BaseDamageMultiplier = 1f;
    [Range(1f, 5f)] public float BerserkDamageMultipler = 1f;

    [Header("Custom values")]
    public float GroundSlamRadius;
    public float BlastForce;

    [HideInInspector]
    public float Damage
    {
        get
        {
            return GameManager.Player.IsBerserking ? (BaseDamage * BaseDamageMultiplier) * BerserkDamageMultipler : (BaseDamage * BaseDamageMultiplier);
        }
        private set
        {
            BaseDamage = value;
        }
    }

    private float DefaultDamage;
    private float DefaultBerserkDamageMultipler;
    private float DefaultGroundSlamRadius;
    private float DefaultBlastForce;

    public override bool StartAbility()
    {        
        if (!GameManager.Player.IsGrounded || !base.StartAbility())        
            return false;

        return true;        
    }

    public override bool Apply(ref UpgradeData data)
    {
        if (!base.Apply(ref data) ||
            data.AbilityUpgrade != UpgradeHub.AbilityType.GroundSlam)
            return false;

        switch (data.UpgradeType)
        {
            case UpgradeHub.UpgradeType.CooldownReduce_p:
                {
                    CooldownTime -= data.Percentage;
                    break;
                }
            case UpgradeHub.UpgradeType.RadiusIncrease_p:
                {
                    GroundSlamRadius += data.Percentage;
                    break;
                }
            default: Debug.LogError("Object: " + this + " does not include method to handle upgrade of type: " + Enum.GetName(typeof(UpgradeHub.UpgradeType), data.UpgradeType) + " - this should not be called on this object"); break;
        }

        return true;
    }

    public override void Reset()
    {
        base.Reset();
        GroundSlamRadius = DefaultGroundSlamRadius;
        BlastForce = DefaultBlastForce;
        BaseDamage = DefaultDamage;
        BerserkDamageMultipler = DefaultBerserkDamageMultipler;
    }

    public override void Setup()
    {
        base.Setup();
        DefaultDamage = BaseDamage;
        DefaultBerserkDamageMultipler = BerserkDamageMultipler;
        DefaultGroundSlamRadius = GroundSlamRadius;
        DefaultBlastForce = BlastForce;
    }
}

[CreateAssetMenu(fileName = "BerserkAbility", menuName = "BerserkAbility")]
[System.Serializable]
public class BerserkAbility : AbilityData
{
    public BerserkAbility(BerserkAbility abilityData)
    {
        if (abilityData != null)
        {
            this.CooldownTime = abilityData.CooldownTime;
            this.UseTime = abilityData.UseTime;
        }
    }

    [Header("Custom values")]
    [Range(5f, 30f)] public float UseTime;

    private float DefaultUseTime;

    public override bool Apply(ref UpgradeData data)
    {
        if (!base.Apply(ref data) ||
            data.AbilityUpgrade != UpgradeHub.AbilityType.Berserk)
            return false;

        switch (data.UpgradeType)
        {
            case UpgradeHub.UpgradeType.CooldownReduce_p:
                {
                    CooldownTime -= data.Percentage;
                    break;
                }
            default: Debug.LogError("Object: " + this + " does not include method to handle upgrade of type: " + Enum.GetName(typeof(UpgradeHub.UpgradeType), data.UpgradeType) + " - this should not be called on this object"); break;
        }

        return true;
    }

    public override void Reset()
    {
        base.Reset();
        UseTime = DefaultUseTime;
    }

    public override void Setup()
    {
        base.Setup();
        DefaultUseTime = UseTime;
    }
}

[CreateAssetMenu(fileName = "TeleportAbility", menuName = "TeleportAbility")]
[System.Serializable]
public class TeleportAbiility : AbilityData
{
    public TeleportAbiility(TeleportAbiility abilityData)
    {
        if (abilityData != null)
        {
            this.CooldownTime = abilityData.CooldownTime;
            this.MaxTeleportDistance = abilityData.MaxTeleportDistance;
        }
    }

    [Header("Custom values")]
    [Range(5f, 25f)] public float MaxTeleportDistance;
    [Range(1f, 5f)] public float MinTeleportDistance;
    [Range(5f, 25f)] public float StartTeleportDistance;

    [Range(5f, 10f)] public float ScrollMultiplier = 9f;
    [Range(170f, 230f)]public float TeleportSpeed;
    [Header("Tags for climbable objects eg. floor and stone courtyard")]
    public string[] ClimbableTags;
    [Header("Tags for environment objects which cannot be teleported onto")]
    public string[] EnvironmentTags;

    public int[] CollisionIgnoreLayers;    
    public GameObject TestPosition;
    public Transform TeleportPosition;
    [Header("Not in yet")]
    [Range(55, 90)] public float FOVKickback = 60f;

    private float DefaultMaxTeleportDistance;

    public override bool Apply(ref UpgradeData data)
    {
        if (!base.Apply(ref data) ||
            data.AbilityUpgrade != UpgradeHub.AbilityType.Teleport)
            return false;

        switch (data.UpgradeType)
        {
            case UpgradeHub.UpgradeType.CooldownReduce_p:
                {
                    CooldownTime -= data.Percentage;
                    break;
                }
            case UpgradeHub.UpgradeType.MaxDistance_p:
                {
                    MaxTeleportDistance += data.Percentage;
                    break;
                }
            default: Debug.LogError("Object: " + this + " does not include method to handle upgrade of type: " + Enum.GetName(typeof(UpgradeHub.UpgradeType), data.UpgradeType) + " - this should not be called on this object"); break;
        }

        return true;
    }

    public override void Reset()
    {
        base.Reset();
        MaxTeleportDistance = DefaultMaxTeleportDistance;
    }

    public override void Setup()
    {
        base.Setup();
        DefaultMaxTeleportDistance = MaxTeleportDistance;
    }
}


[CreateAssetMenu(fileName = "ArrowSprayAbility", menuName = "ArrowSprayAbility")]
[System.Serializable]
public class ArrowSpray : AbilityData
{
    public ArrowSpray(ArrowSpray abilityData)
    {
        if (abilityData != null)
        {
            this.CooldownTime = abilityData.CooldownTime;            
        }
    }

    public ArrowSprayData m_ArrowSprayData;
    [HideInInspector] public bool UseBerserkReducedCooldown = false;
    [Header("Percentage of original value")]
    [Range(.1f, 1f)] public float ReducedBerserkCooldownPercentage = .7f;
    public Transform SpawnPoint;
    public GameObject SprayArrowGO = null;

    public override float CooldownTime
    {
        get
        {
            float temp = base.CooldownTime;
            return UseBerserkReducedCooldown && GameManager.Player.IsBerserking ? (temp * ReducedBerserkCooldownPercentage) : temp;
        }
        set => base.CooldownTime = value;
    }

    private float DefaultReducedBerserkCooldown;

    public override bool Apply(ref UpgradeData data)
    {
        if (!base.Apply(ref data) ||
            data.AbilityUpgrade != UpgradeHub.AbilityType.ArrowSpray)
            return false;

        switch (data.UpgradeType)
        {
            case UpgradeHub.UpgradeType.CooldownReduce_p:
                {
                    CooldownTime -= data.Percentage;
                    break;
                }
            case UpgradeHub.UpgradeType.IncreaseArrowSprayArrows_i:
                {
                    m_ArrowSprayData.ArrowsToShoot += data.IntValue;
                    break;
                }
            default: Debug.LogError("Object: " + this + " does not include method to handle upgrade of type: " + Enum.GetName(typeof(UpgradeHub.UpgradeType), data.UpgradeType) + " - this should not be called on this object"); break;
        }

        return true;
    }

    public override void Reset()
    {
        base.Reset();
        UseBerserkReducedCooldown = false;
        ReducedBerserkCooldownPercentage = DefaultReducedBerserkCooldown;
        m_ArrowSprayData.Reset();
    }

    public override void Setup()
    {
        base.Setup();
        DefaultReducedBerserkCooldown = ReducedBerserkCooldownPercentage;
        m_ArrowSprayData.Setup();
    }
}


public class Abilities : MonoBehaviour
{
    public enum AbilityTypes { Charge, GroundSlam, Berserk, Teleport, ArrowSpray }
    public enum AbilityUpgrade { Damage, Speed, Cooldown, Usetime, UnlockFunctionality }
    public enum AbilityDataType { Cooldown, UsingAbility } 
    public int NumOfAbilities { get; private set; } = 0;

    [SerializeField] Animator SwordAnimator = null;
    [SerializeField] Animator BowAnimator = null;

    [SerializeField] ChargeAbility chargeAbility = null;
    [SerializeField] GroundSlamAbility groundSlamAbility = null;
    [SerializeField] BerserkAbility berserkAbility = null;
    [SerializeField] TeleportAbiility teleportAbility = null;
    [SerializeField] ArrowSpray arrowSprayAbility = null;

    private Dictionary<AbilityTypes, AbilityData> AbilityDataDict;
    private FirstPersonController Player;

    public UnityEvent StartCharge;
    public UnityEvent WallHit;
    public UnityEvent EnemyHit;
    public UnityEvent ChargeEnd;

    private enum ChargeCollisionFlag
    {
        Nothing,
        Environment,
        Enemy
    }

    private ChargeCollisionFlag chargeCollisionFlag;

    public AbilityData GetAbilityData(AbilityTypes type)
    {
        if (AbilityDataDict.ContainsKey(type))
        {
            var data = AbilityDataDict[type];
            if (data != null)
                return data;
        }

        return null;
    }

    public T GetAbilityData<T>(AbilityTypes type, AbilityDataType dataType)
    {
        if (AbilityDataDict.ContainsKey(type))
        {
            var data = AbilityDataDict[type];
            switch(dataType)
            {
                case AbilityDataType.Cooldown:
                    return (T)Convert.ChangeType(data.OnCooldown, typeof(T));

                case AbilityDataType.UsingAbility:
                    return (T)Convert.ChangeType(data.UsingAbility, typeof(T));
            }          
        }
        return (T)Convert.ChangeType(null, typeof(T));
    }

    //public bool IsUsingAbilityType(AbilityTypes type)
    //{
    //    if (AbilityDataDict.ContainsKey(type) && AbilityDataDict[type].UsingAbility)
    //        return true;

    //    return false;
    //}

    [HideInInspector]
    public bool IsAnyAbilityBeingUsed
    {
        get
        {
            foreach (KeyValuePair<AbilityTypes, AbilityData> abilityData in AbilityDataDict)
            {
                if (abilityData.Key == AbilityTypes.Berserk)
                    continue;

                if (abilityData.Value.UsingAbility)
                    return true;
            }
            return false;
        }
    }

    public bool IsCharging
    {
        get
        {
            return chargeAbility.UsingAbility;
        }
    }

    public bool IsBerserking
    {
        get
        {
            return berserkAbility.UsingAbility;
        }      
    }

    public bool IsTeleporting
    {
        get
        {
            return teleportAbility.UsingAbility;
        }
    }  

    public bool OnChargeCooldown
    {
        get
        {
            return chargeAbility.OnCooldown;
        }   
    }

    public void Setup()
    {
        try
        {
            if (!SwordAnimator || !BowAnimator)
            {
                Debug.LogError("Weapon animators are not supplied in abilities");
            }

            NumOfAbilities = Enum.GetNames(typeof(AbilityTypes)).Length;

            Player = GameManager.Player;
            AbilityDataDict = new Dictionary<AbilityTypes, AbilityData>(NumOfAbilities);

            AbilityDataDict.Add(AbilityTypes.Charge, chargeAbility);
            AbilityDataDict.Add(AbilityTypes.GroundSlam, groundSlamAbility);
            AbilityDataDict.Add(AbilityTypes.Berserk, berserkAbility);
            AbilityDataDict.Add(AbilityTypes.Teleport, teleportAbility);
            AbilityDataDict.Add(AbilityTypes.ArrowSpray, arrowSprayAbility);

            foreach (KeyValuePair<AbilityTypes, AbilityData> ability in AbilityDataDict)
            {
                ability.Value.Setup();
            }
        }
        catch(Exception e)
        {
            Debug.LogException(e, this);
            Debug.LogError("Abilities setup failed");
        }      
    }   

    public void StartAbility(AbilityTypes type)
    {
        if (AbilityDataDict.ContainsKey(type))
            if (!AbilityDataDict[type].StartAbility())
                return;

        switch (type)
        {
            case AbilityTypes.Berserk: { StartCoroutine(Berserk()); break; }
            case AbilityTypes.Charge: { Charge(); break; }
            case AbilityTypes.GroundSlam: { CallGroundSlamAnimation(); break; }
            case AbilityTypes.Teleport: { StartCoroutine(Teleport()); break; }
            default: Debug.LogError("Ability type: " + Enum.GetName(typeof(AbilityTypes), type) + " is not linked to relevant methods"); break;
        }
    }

    public bool ResetAllAbilities()
    {
        if (AbilityDataDict != null)
        {
            foreach (KeyValuePair<AbilityTypes, AbilityData> ability in AbilityDataDict)
            {
                ability.Value.Reset();
            }
            return true;
        }

        return false;
    }


    public void Charge()
    {
        if (Player.IsGrounded)
        {
            Player.m_IsAttacking = true;
            chargeAbility.UsingAbility = true;
            StartCharge.Invoke();
            StartCoroutine(ChargeMovement());           
        }
    }

    private IEnumerator ChargeMovement()
    {
        float timer = 0f;
        Player.m_IsAttacking = true;
        Player.m_CanMove = false;
        chargeAbility.ChargeCollider.enabled = true;

        while (timer < chargeAbility.ChargeTime && chargeAbility.UsingAbility)
        {
            timer += Time.deltaTime;
            Vector3 v = transform.forward * chargeAbility.ChargeThrust * Time.deltaTime;
            Player.MoveController(v);

            yield return null;
        }

        ChargeEnd.Invoke();
        Player.m_IsAttacking = false;
        Player.m_CanMove = true;
        chargeAbility.ChargeCollider.enabled = false;
        // Start Cooldown
        StartCoroutine(AbilityCooldowns(AbilityTypes.Charge));
        yield break;
    }

    public IEnumerator Berserk()
    {   
        Debug.Log("Start Berserk");
        berserkAbility.UsingAbility = true;
        float timer = 0;

        while (timer < berserkAbility.UseTime)
        {
            timer += Time.deltaTime;
            Debug.Log("Debug Timer: " + timer + " out of use time of: " + berserkAbility.UseTime);
            yield return null;
        }
        
        //yield return new WaitForSeconds(berserkAbility.UseTime);
        Debug.Log("End Berserk");

        StartCoroutine(AbilityCooldowns(AbilityTypes.Berserk));
    }

    // initial call
    public void CallGroundSlamAnimation()
    {
        Player.m_IsAttacking = true;
        SwordAnimator.SetTrigger("GroundSlam");
    }    

    public void GroundSlam()
    {       
        groundSlamAbility.UsingAbility = true;

        Collider[] overlapColliders = Physics.OverlapSphere(Player.transform.position, groundSlamAbility.GroundSlamRadius);

        foreach (Collider collider in overlapColliders)
        {
            GameObject hitObj = collider.gameObject;
            if (GameManager.IsTagAnEnemy(hitObj.tag))
            {
                //Enemy enemy = hitObj.GetComponent<Enemy>();
                //enemy.rb.isKinematic = false;

                //Vector3 pushForce = hitObj.transform.position - (Player.transform.position - new Vector3(0, -5, 0));
                //pushForce.Normalize();
                //pushForce *= groundSlamAbility.BlastForce;

                //enemy.ThrowBack(pushForce);
                hitObj.GetComponent<Enemy>().TakeDamage(groundSlamAbility.Damage);
            }
        }
        StartCoroutine(AbilityCooldowns(AbilityTypes.GroundSlam));
    }

    public IEnumerator Teleport()
    {     
        Player.m_IsAttacking = true;
        teleportAbility.UsingAbility = true;
        teleportAbility.TestPosition.gameObject.SetActive(true);
        float MaxTeleportDistance = teleportAbility.MaxTeleportDistance;
        float MinTeleportDistance = teleportAbility.MinTeleportDistance;
        float TeleportDistance = teleportAbility.StartTeleportDistance;
        float ScrollMultiplier = teleportAbility.ScrollMultiplier;

        float FOV = Camera.main.fieldOfView;
        float FOVKickback = teleportAbility.FOVKickback;
        float FOVDiff = FOVKickback - FOV;
        int teleportLayerMask = GameManager.PlayerIgnoreLayerMask | 1 << GameManager.m_EnemyLayer;


        // if raycast didnt hit telportable object then take the end position of the ray and cast downwards onto the ground to get the ground position
        // if hit wall then move that rayend point check backwards by half the radius of the player's controller

        while (teleportAbility.UsingAbility)
        {          
            if (Input.GetKeyDown(KeyBinds.Attack))
            {               
                Player.m_CanMove = false;
                Player.CanCollide = false;

                float startTime = Time.time;
                float journeyLength = Vector3.Distance(Player.m_AnchorPoint.position, teleportAbility.TeleportPosition.position);            

                while (true)
                {                    
                    float distCovered = (Time.time - startTime) * teleportAbility.TeleportSpeed;
                    float totalJourney = distCovered / journeyLength;

                    Vector3 lerpMovement = Vector3.Lerp(Player.m_AnchorPoint.position, teleportAbility.TeleportPosition.position, totalJourney);                
                    Player.MoveTransform(lerpMovement);

                    if (totalJourney >= 1)
                        break;

                    yield return null;
                }

                Player.m_CanMove = true;
                Player.CanCollide = true;
                Player.m_IsAttacking = false;
                teleportAbility.TestPosition.gameObject.SetActive(false);
                StartCoroutine(AbilityCooldowns(AbilityTypes.Teleport));
                yield break;

            }

            if (Input.GetKeyDown(KeyBinds.CancelTeleport))
            {
                Player.m_IsAttacking = false;
                teleportAbility.UsingAbility = false;
                teleportAbility.TestPosition.gameObject.SetActive(false);
                yield break;
            }

            float ScrollValue = Input.GetAxis("Mouse ScrollWheel");

            if (ScrollValue != 0)
            {
                if (ScrollValue > 0)
                {
                    TeleportDistance -= ScrollValue * -ScrollMultiplier;
                    if (TeleportDistance > MaxTeleportDistance)
                        TeleportDistance = MaxTeleportDistance;
                }
                else if (ScrollValue < 0)
                {
                    TeleportDistance += ScrollValue * ScrollMultiplier;
                    if (TeleportDistance < MinTeleportDistance)
                        TeleportDistance = MinTeleportDistance;                  
                }
            }
            
            Ray ray = new Ray(Camera.main.transform.position, Camera.main.transform.forward/*Player.ControllerForward*/);
            RaycastHit hit;

            bool climbableObjectHitSuccessful = false;

            // if initial hit
            if (Physics.Raycast(ray, out hit, TeleportDistance, teleportLayerMask))
            {
                GameObject hitObj = hit.collider.gameObject;

                for (int i = 0; i < teleportAbility.ClimbableTags.Length; i++)
                {
                    if (hitObj.tag == teleportAbility.ClimbableTags[i])
                    {
                        // teleportable objects initial hit
                        teleportAbility.TeleportPosition.position = hit.point;
                        climbableObjectHitSuccessful = true;
                    }                   
                }

                if (!climbableObjectHitSuccessful)
                {                             
                    // test if object was non navigatable and if so find new offset position
                    for (int i = 0; i < teleportAbility.EnvironmentTags.Length; i++)
                    {
                        if (hitObj.tag == teleportAbility.EnvironmentTags[i])
                        {
                            float distance = Vector3.Distance(Camera.main.transform.position, hit.point);

                            Vector3 reducedPos = (ray.origin + (ray.direction * (distance - Player.m_ControllerRadius)));

                            // down raycast with new position relative to width of character controller
                            DownCast(ref reducedPos);
                        }
                    }                                              
                }                        
            }
            // failed any initial raycasts
            else
            {              
                Vector3 DownCastPoint = ray.origin + (ray.direction * TeleportDistance);
                DownCast(ref DownCastPoint);                
            }            

            if (teleportAbility.TestPosition)
                teleportAbility.TestPosition.transform.position = teleportAbility.TeleportPosition.position;

            // raycast down to hit ground
            void DownCast(ref Vector3 pos)
            {
                if (Physics.Raycast(pos, Vector3.down, out hit, 100))
                {
                    GameObject hitObj = hit.collider.gameObject;

                    for (int i = 0; i < teleportAbility.ClimbableTags.Length; i++)
                    {
                        if (hitObj.tag == teleportAbility.ClimbableTags[i])
                        {
                            teleportAbility.TeleportPosition.position = hit.point;
                        }
                    }
                }
            }

            yield return null;
        }    
    }

    public void InitialArrowSpray()
    {
        arrowSprayAbility.UsingAbility = true;
        Player.m_IsAttacking = true;
    }

    public void ArrowSpray()
    {
        var data = arrowSprayAbility.m_ArrowSprayData;
        if (data == null)
            Debug.LogError("Arrowspray data is unnasigned in abilties");

        if (!data.Prefab)
        {
            Debug.LogError("Arrowspray prefab is unassigned");
            return;
        }

        // arrow arc / number of arrows, flexible with easily adding less/more arrows
        // start from position and rotation then gradually rotate position by new arcMovement to get interval shot points

        // if we have the normal forward of the arrows, can use that to angle rotation of spawnpoint by half the arc rotation to get the full arc
        float arc = data.ShotArc;
        // must be pointed along arrows forward vector
        Transform SpawnPoint = arrowSprayAbility.SpawnPoint;
        float newAngleRot = arc / data.ArrowsToShoot;

        Quaternion baseRotation = SpawnPoint.rotation * Quaternion.Euler(0, arc / 2 + newAngleRot / 2, 0);
        Quaternion newNewRot = baseRotation;

        for (int i = 0; i < data.ArrowsToShoot; i++)
        {
            Quaternion newRotation = newNewRot * Quaternion.Euler(0, -newAngleRot, 0 );
            newNewRot = newRotation;

            Arrow arrow = Instantiate(data.Prefab, SpawnPoint.position, newRotation);
            StartCoroutine(arrow.ArrowFire(data));
        }
    }

    public void SetArrowSprayAttackingFalse()
    {        
        StartCoroutine(AbilityCooldowns(AbilityTypes.ArrowSpray));
        Player.m_IsAttacking = false;
    }

    private IEnumerator AbilityCooldowns(AbilityTypes type)
    {
        float timer = 0f;

        var AbilityData = AbilityDataDict[type];
        float Cooldowntime = AbilityData.CooldownTime;

        AbilityData.UsingAbility = false;
        AbilityData.OnCooldown = true;

        while (timer < Cooldowntime)
        {
            if (AbilityData.CooldownImage)
            {
                float inverted = 1.0f - (timer / Cooldowntime);
                AbilityData.CooldownImage.fillAmount = inverted;
            }             
               
            timer += Time.deltaTime;
            yield return null;
        }

        if (AbilityData.CooldownImage != null)
            AbilityData.CooldownImage.fillAmount = 0f;

        AbilityData.OnCooldownFinishEvent.Invoke();
        AbilityData.OnCooldown = false;    
    }

    // Oncharge collision detection
    void OnTriggerEnter(Collider collider)
    {
        if (chargeAbility.UsingAbility)
        {
            GameObject hitObj = collider.gameObject;

            for (int i = 0; i < chargeAbility.ChargeHitTags.Length; i++)
            {
                // all environment hits
                if (hitObj.tag == chargeAbility.ChargeHitTags[i])
                {
                    WallHit.Invoke();
                    chargeCollisionFlag = ChargeCollisionFlag.Environment;
                    chargeAbility.UsingAbility = false;
                }
            }

            if (GameManager.IsTagAnEnemy(hitObj.tag))
            {
                EnemyHit.Invoke();
                chargeCollisionFlag = ChargeCollisionFlag.Enemy;
                hitObj.SendMessage("TakeDamage", chargeAbility.Damage, SendMessageOptions.DontRequireReceiver);
                chargeAbility.UsingAbility = false;
            }          
        }
    }   


    public void ChargeImpactSound()
    {
        string SoundString = "ChargeImpact";

        switch (chargeCollisionFlag)
        {
            case ChargeCollisionFlag.Nothing: { SoundString += "Nothing"; break; }
            case ChargeCollisionFlag.Environment: { SoundString += "Env"; break; }
            case ChargeCollisionFlag.Enemy: { SoundString += "Enemy"; break; }              
        }
        Player.SendMessage("PlaySoundFromEvent", SoundString, SendMessageOptions.DontRequireReceiver);

        chargeCollisionFlag = ChargeCollisionFlag.Nothing;
    }
}

