﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeybindManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(gameObject);
    }

    public void RebindForward(KeyCode key)
    {
        KeyBinds.Forward = key;
    }

    public void RebindBack(KeyCode key)
    {
        KeyBinds.Back = key;
    }

    public void RebindLeft(KeyCode key)
    {
        KeyBinds.Left = key;
    }

    public void RebindRight(KeyCode key)
    {
        KeyBinds.Right = key;
    }

    public void RebindJump(KeyCode key)
    {
        KeyBinds.Jump = key;
    }

    public void RebindAttack(KeyCode key)
    {
        KeyBinds.Attack = key;
    }

    public void RebindWeapon1(KeyCode key)
    {
        KeyBinds.Weapon1 = key;
    }

    public void RebindWeapon2(KeyCode key)
    {
        KeyBinds.Weapon2 = key;
    }

    public void RebindAbility1(KeyCode key)
    {
        KeyBinds.Ability1 = key;
    }

    public void RebindAbility2(KeyCode key)
    {
        KeyBinds.Ability2 = key;
    }

    public void RebindMobility(KeyCode key)
    {
        KeyBinds.Mobility = key;
    }

    public void RebindUlt(KeyCode key)
    {
        KeyBinds.Ult = key;
    }
}
