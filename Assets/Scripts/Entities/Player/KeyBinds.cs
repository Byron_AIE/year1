﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
How to Use:
    Instead of using (Input.GetKeyDown(KeyCode.W)) or (Input.GetButtonDown("Forward"))
    Use (Input.GetKeyDown(KeyBinds.Forward))
    No need for a reference, this class is static

    Key Bindings can be changed with the KeybindManager
*/

public static class KeyBinds
{
    public static KeyCode Forward { get; set; }
    public static KeyCode Back { get; set; }
    public static KeyCode Left { get; set; }
    public static KeyCode Right { get; set; }
    public static KeyCode Jump { get; set; }
    public static KeyCode Attack { get; set; }
    public static KeyCode Weapon1 { get; set; }
    public static KeyCode Weapon2 { get; set; }
    public static KeyCode Ability1 { get; set; }
    public static KeyCode Ability2 { get; set; }
    public static KeyCode Mobility { get; set; }
    public static KeyCode Ult { get; set; }
    public static KeyCode KillAll { get; set; }
    public static KeyCode CancelTeleport { get; set; }
    public static KeyCode ChangeWeapon { get; set; }

    static KeyBinds()
    {
        Forward = KeyCode.W;
        Back = KeyCode.S;
        Left = KeyCode.A;
        Right = KeyCode.D;
        Jump = KeyCode.Space;
        Attack = KeyCode.Mouse0;
        Weapon1 = KeyCode.Alpha1;
        Weapon2 = KeyCode.Alpha2;
        Ability1 = KeyCode.Q;
        Ability2 = KeyCode.E;
        Mobility = KeyCode.LeftShift;
        Ult = KeyCode.R;
        KillAll = KeyCode.K;
        CancelTeleport = KeyCode.Mouse1;
        ChangeWeapon = KeyCode.Tab;
    }
}
