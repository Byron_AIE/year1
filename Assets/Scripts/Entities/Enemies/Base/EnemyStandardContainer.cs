﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[CreateAssetMenu(fileName = "New Enemy Standard Container", menuName = "Enemy Standard Container")]
[System.Serializable]
public class EnemyStandardContainer : EnemyContainerBase
{
    public EnemyStandardContainer() { }
    public EnemyStandardContainer(EnemyStandardContainer other)
    {
        Prefab = other.Prefab;
        Health = other.Health;
        Damage = other.Damage;
        MovementSpeed = other.MovementSpeed;
        AccelerationSpeed = other.AccelerationSpeed;
        AngularMovementSpeed = other.AngularMovementSpeed;
        EnemyType = other.EnemyType;
        NewWaveData = other.NewWaveData;
        SpawnWeighting = other.SpawnWeighting;
    }

    // combine containers and use enum checking to verify which data to grab

    [Range(0, 20)]
    public float SpawnWeighting;
}