﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;
using UnityStandardAssets.Characters.FirstPerson;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(AudioSource))]
public class Enemy : MonoBehaviour, IEntities<float>
{
   // protected enum EnemySounds { attack, takedamage, death }
   // protected Dictionary<EnemySounds, AudioClip[]> audioClips;
   //
    protected enum EnemyStates
    {
        Stationary,
        MoveTowardsPlayer,
        FleeFromPlayer,
        SurroundPlayer,
        Attack,
        Stunned,
        Dead
    }
    private EnemyStates state = EnemyStates.Stationary;
    protected virtual EnemyStates State
    {
        get
        {
            return state;
        }
        set
        {
            state = value;
        }
    }

    // Enemy stats
    protected float m_movementSpeed;
    protected float m_health;
    protected float m_damage;

    //[SerializeField] protected EnemyStandardContainer Data;

    [SerializeField] AudioClip[] m_DeathSounds = null;
    [SerializeField] AudioClip[] m_AttackSounds = null;
    [SerializeField] AudioClip[] m_TakeDamageSounds = null;
    protected WaveManager m_wavemanager = null;

    // Component References
    protected AudioSource m_audioSource;
    protected Animator m_animator;
    protected NavMeshAgent m_agent;
    protected Rigidbody m_rb;
    protected Renderer m_meshRenderer;
    protected FirstPersonController m_player;
    protected bool m_lookAtPlayer = true;
    protected bool m_isAttacking = false;
    protected EnemyStates m_oldState;

    private const int m_obstacleLayerMask = 1 << 10;
    private Collider[] m_hits = new Collider[32];
    private List<Vector3> m_transforms = new List<Vector3>();
    private List<Vector3> m_avoidTransforms = new List<Vector3>();

    public UnityEvent m_TakeDamageEvent;
    public UnityEvent m_DeathEvent;
    public UnityEvent m_IdleEvent;
    public UnityEvent m_RunEvent;
    public UnityEvent m_AttackEvent;
    public UnityEvent m_EnterCombatEvent;

    CombatMusic combatMusic;

    public float Damage
    {
        get
        {
            return m_damage;
        }
    }

    public void SetPosition(Vector3 position)
    {
        m_agent.Warp(position);
    }

    private void Awake()
    {
        try
        {
            m_agent = GetComponent<NavMeshAgent>();
            m_animator = GetComponent<Animator>();
            m_meshRenderer = GetComponent<Renderer>();
            m_audioSource = GetComponent<AudioSource>();
            m_rb = GetComponent<Rigidbody>();
            combatMusic = FindObjectOfType<CombatMusic>();
        }
        catch (Exception e)
        {
            Debug.LogException(e, this);
            Debug.LogError(this.name + " setup incomplete - Missing components");
            return;
        }

        m_movementSpeed = 2f;

        m_agent.updateRotation = false;
        m_player = GameManager.Player;
        m_wavemanager = GameManager.Wavemanager;
        gameObject.transform.position = Vector3.zero;
        gameObject.SetActive(false);
    }

    protected virtual void Update()
    {
        if (m_lookAtPlayer)
        {
            Vector3 look = new Vector3(m_player.transform.position.x, transform.position.y, m_player.transform.position.z);
            transform.LookAt(look);
            //agent.updateRotation = false;
            //Vector3 look = new Vector3(agent.nextPosition.x, agent.nextPosition.y/*player.transform.position.y*/, agent.nextPosition.z);
            //agent.angularSpeed = 20;
            //agent.acceleration = 20;
            //agent.updateRotation = false;
            //transform.LookAt(look);           
        }
    }

    protected virtual void Attack()
    {

    }

    /// <summary>
    /// Setup initial values for enemy
    /// </summary>
    /// <param name="enemy"></param>
    /// <param name="dataContainer"></param>
    /// <returns>
    /// Returns true if no exception thrown
    /// </returns>
    public virtual bool Setup(EnemyContainerBase dataContainer)
    {
        //Debug.Assert(enemy != null || dataContainer != null, "Enemy setup classes are null");
       
        try
        {
            //NavMeshAgent agent = enemy.GetComponent<NavMeshAgent>();
            m_agent.speed = dataContainer.MovementSpeed;
            m_agent.acceleration = dataContainer.AccelerationSpeed;
            m_agent.angularSpeed = dataContainer.AngularMovementSpeed;
            
            m_health = dataContainer.Health;
            m_damage = dataContainer.Damage;
            m_movementSpeed = dataContainer.MovementSpeed;
        }
        catch (Exception e)
        {
            Debug.LogException(e, this);
            Debug.LogError("Setup failed for " + name, gameObject);

            return false;
        }

        if (m_health <= 0)
        {
            Debug.LogError("Enemy: " + name + " was passed: " + dataContainer.Health + " health in setup");
        }

        return true;
    }

    virtual public void Move()
    {

    }

    public virtual void TakeDamage(float damage)
    {
        if (!gameObject.activeSelf || State == EnemyStates.Dead)
            return;

        if (damage >= 0)
        {
            m_health -= damage;
            //Debug.Log("Ouch: " + damage);
        }
        else
            Debug.LogError("Damage was negative for: " + name);

        if (m_health > 0)
        {
            // Example of controlling animations within script
            //if (animator != null)
            //{
            //    animator.SetBool("Attacked", true);
            //}
            state = EnemyStates.Stunned;
            m_TakeDamageEvent.Invoke();
            if (combatMusic.dmgTakenThisWave == false)
            {
                combatMusic.playCombatMusic = true;
                combatMusic.dmgTakenThisWave = true;
            }

        }
        else
        {
            m_health = 0;

            if (combatMusic.dmgTakenThisWave == false)
            {
                combatMusic.playCombatMusic = true;
                combatMusic.dmgTakenThisWave = true;
            }

            Death();
            // Enemy is dead
        }
    }

    virtual protected void Death()
    {
        m_wavemanager.AddCorpse(this);
        //meshRenderer.enabled = false;
        m_agent.enabled = false;
        //rb.isKinematic = true;
        if (gameObject.activeSelf)
        {
            m_DeathEvent.Invoke();
			//StartCoroutine(CountdownToInactive(audioSource.clip));
		}
		state = EnemyStates.Dead;
		gameObject.SetActive(false);
    }

    protected IEnumerator CountdownToInactive(AudioClip clip)
    {
        float timer = 0f;
        if (clip != null)
            timer = clip.length;
        while (true)
        {
            timer -= Time.deltaTime;
            if (timer <= 0f)
            {
                gameObject.SetActive(false);
                yield break;
            }
            yield return null;
        }
    }

    protected List<Vector3> CalculateNeighbors(float range)
    {
        m_transforms.Clear();
        int found = Physics.OverlapSphereNonAlloc(transform.position, range, m_hits);

        for (int i = 0; i < found; i++)
        {
            if (GameManager.IsTagAnEnemy(m_hits[i].tag))
            {
                if (!(m_hits[i].gameObject == gameObject))
                    m_transforms.Add(m_hits[i].transform.position);
            }
            else if (m_hits[i].tag == "Environment")
            {
                if (Physics.Raycast(transform.position, m_hits[i].transform.position - transform.position, out RaycastHit hit, range))
                    m_transforms.Add(hit.point);
            }
        }
        return m_transforms;
    }

    protected Vector3 CalculateAvoidance(float range)
    {
        Vector3 direction = Vector3.zero;
        m_avoidTransforms = CalculateNeighbors(range);
        if (m_avoidTransforms.Count != 0)
        {
            foreach (Vector3 vec in m_avoidTransforms)
            {
                direction += (transform.position - vec);
            }
            //direction += (player.transform.position - transform.position).normalized * 2f;
            direction /= (m_avoidTransforms.Count);
            direction.y = 0f;
            //direction -= transform.position;
        }
        return direction;
    }
}
