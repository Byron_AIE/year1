﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[CreateAssetMenu(fileName = "New Enemy Interval Container", menuName = "Enemy Interval Container")]
[System.Serializable]
public class EnemyIntervalContainer : EnemyContainerBase
{
    public EnemyIntervalContainer() { }
    public EnemyIntervalContainer(EnemyIntervalContainer other)
    {
        Prefab = other.Prefab;
        Health = other.Health;
        Damage = other.Damage;
        MovementSpeed = other.MovementSpeed;
        AccelerationSpeed = other.AccelerationSpeed;
        AngularMovementSpeed = other.AngularMovementSpeed;
        EnemyType = other.EnemyType;
        NewWaveData = other.NewWaveData;
        SpawnInterval = other.SpawnInterval;
    }

    [Range(0, 20)]
    public int SpawnInterval;
}