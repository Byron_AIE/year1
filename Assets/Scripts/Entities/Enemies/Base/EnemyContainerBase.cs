﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class NewWaveEnemyData
{    
    [Range(1, 100)] public float HealthIncrease;
    [Range(1, 100)] public float DamageIncrease;
}

//[CreateAssetMenu(fileName = "New Enemy Container", menuName = "Enemy Container")]
[System.Serializable]
public class EnemyContainerBase 
{
    public Enemy Prefab;

    [Range(0, 500)] public float Health;
    [Range(0, 500)] public float Damage;
    [Range(0, 20)] public float  MovementSpeed;
    [Range(0, 50)] public float  AccelerationSpeed;
    [Range(0, 50)] public float  AngularMovementSpeed;

    public WaveManager.EnemyTypes EnemyType;
    public NewWaveEnemyData NewWaveData = null;

    public virtual void UpgradeStats()
    {
        Health += NewWaveData.HealthIncrease;
        Damage += NewWaveData.DamageIncrease;
    }
}
