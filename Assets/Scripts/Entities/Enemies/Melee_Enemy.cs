﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Melee_Enemy : Enemy
{
    [Range(0.1f, 5f)]
    public float avoidanceRange = 1f;
    [Range(1f, 8f)]
    public float standBackRange = 4f;
    [Range(0.001f, 2f)]
    public float movementStep = 0.03f;

    private float standingDistance;
    [SerializeField] Collider weaponHitbox;
    
    private void Start()
    {
        standingDistance = standBackRange * 0.75f;
        State = EnemyStates.Stationary;
        avoidanceRange = Random.Range(0.5f, 2f);
        try
        {
            ColliderHolder h = GetComponent<ColliderHolder>();
            weaponHitbox = h.colliderToUse;
            weaponHitbox.enabled = false;
        }
        catch
        {
            Debug.LogError("Collider holder not found on " + name + ". Please attach a ColliderHolder to the enemy and assign a hitbox", gameObject);
        }
        weaponHitbox.enabled = false;
    }

    // Update is called once per frame
    override protected void Update()
    {       
        if (State != EnemyStates.Dead && State != EnemyStates.Stunned)
        {
            base.Update();

            if (Vector3.SqrMagnitude(m_player.transform.position - transform.position) > standBackRange * standBackRange)
            {
                State = EnemyStates.MoveTowardsPlayer;
            }
            else
            {
                // If enemy can attack or this enemy is already attacking
				if (EnemyManager.CanEnemyAttack || m_isAttacking)
				{
					State = EnemyStates.Attack;
				}
				else
					State = EnemyStates.SurroundPlayer;
            }
        }
        Move();
    }

    protected override EnemyStates State
    {
        get
        {
            return base.State;
        }
        set
        {
            // if moving into attack state
            if (base.State != EnemyStates.Attack && value == EnemyStates.Attack)
            {
                EnemyManager.attackingEnemies++;
                m_isAttacking = true;
            }
            // if moving out of attack state
            else if (base.State == EnemyStates.Attack && value != EnemyStates.Attack)
            {
                EnemyManager.attackingEnemies--;
                m_isAttacking = false;
            }
            base.State = value;
        }
        
    }

    override public void Move()
    {
        Vector3 direction;
        bool changedState = (m_oldState != State);
        switch (State)
        {
            case EnemyStates.Stationary:
                if (changedState)
                    m_IdleEvent.Invoke();
                m_agent.isStopped = true;
                direction = CalculateAvoidance(avoidanceRange);
                transform.position = (Vector3.MoveTowards(transform.position, transform.position + direction.normalized, movementStep));
                break;

            case EnemyStates.Stunned:
                m_agent.isStopped = true;
                break;

            case EnemyStates.MoveTowardsPlayer:
                m_agent.isStopped = false;
                if (changedState)
                    m_RunEvent.Invoke();
                direction = CalculateAvoidance(avoidanceRange);
                transform.position = (Vector3.MoveTowards(transform.position, transform.position + direction.normalized, movementStep));
                NavMeshHit hit;
                if (m_agent.enabled && NavMesh.SamplePosition(transform.position, out hit, 3f, NavMesh.AllAreas))
                    m_agent.SetDestination(m_player.transform.position);
                break;

            case EnemyStates.SurroundPlayer:
                m_agent.isStopped = true;
                if (changedState)
                   m_IdleEvent.Invoke();
                direction = transform.position - m_player.transform.position;
                direction += CalculateAvoidance(avoidanceRange);
                direction.Normalize();
                Vector3 point = m_player.transform.position + direction * standingDistance;
                transform.position = Vector3.MoveTowards(transform.position, point.normalized, movementStep);
                break;

            case EnemyStates.Attack:
                if (changedState)
                    m_RunEvent.Invoke();
                m_agent.isStopped = false;
                m_agent.SetDestination(m_player.transform.position);
                m_agent.stoppingDistance = 1.9f;
                if (m_agent.remainingDistance <= 2f)
                {
                    m_agent.isStopped = true;
                    Attack();
                }       
                break;

            case EnemyStates.Dead:
                m_agent.isStopped = true;
                break;
        }
        m_oldState = State;
    }

    protected override void Attack()
    {
        base.Attack();
        m_AttackEvent.Invoke();
        Debug.Log("GruntAttack");

        //StartCoroutine(AttackTimer(1f));
    }

    ////Temporary attack timer for attacks while animations are being made
    //IEnumerator AttackTimer(float waitTime)
    //{
    //    float timer = 0f;
    //    lookAtPlayer = false;
    //    while (true)
    //    {
    //        timer += Time.deltaTime;
    //        if (timer >= waitTime)
    //        {
    //            //After timer, activate damage hitbox
    //            weaponHitbox.enabled = true;
    //            break;
    //        }
    //        yield return null;
    //    }
    //    timer = 0f;
    //    while (true)
    //    {
    //        timer += Time.deltaTime;
    //        if (timer >= waitTime / 2)
    //        {
    //            //After shorter timer, disable damage hitbox
    //            weaponHitbox.enabled = false;
    //            lookAtPlayer = true;
    //            yield break;
    //        }
    //        yield return null;
    //    }
    //}

    override protected void Death()
    {
		// Custom death code
		State = EnemyStates.Dead;
		base.Death();
    }

    override public void TakeDamage(float damage)
    {
        if (State != EnemyStates.Stunned)
            StartCoroutine(StunTimer(1f));
        base.TakeDamage(damage);
    }

    public void ActivateSpearCollider()
    {
        weaponHitbox.enabled = true;
    }

    public void DisableSpearCollider()
    {
        weaponHitbox.enabled = false;
    }

    private IEnumerator StunTimer(float length)
    {
        float timer = 0f;
        State = EnemyStates.Stunned;
        while (true)
        {
            timer += Time.deltaTime;
            if (timer >= length)
            {
                State = EnemyStates.Stationary;
                yield break;
            }
            yield return null;
        }
    }
}
