﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kamikaze_Enemy : Enemy
{
    // Update is called once per frame
    void Update()
    {

    }

    override protected void Death()
    {

        base.Death();
    }

    override public void TakeDamage(float damage)
    {
        base.TakeDamage(damage);

        if (m_health <= 0)
        {
            m_health = 0;
            Death();
            // Enemy is dead
        }
    }
}
