﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmourRandomizer : MonoBehaviour
{
    [Tooltip("Probability of any piece of armour appearing")] [Range(0, 100)] public int probabilityOfArmour;
    public Renderer[] armourPieces;
    // Start is called before the first frame update
    void Awake()
    {
        for (int i = 0; i < armourPieces.Length; i++)
        {
            int number = Random.Range(0, 100);
            if (number > probabilityOfArmour)
                armourPieces[i].enabled = false;
        }        
    }
}
