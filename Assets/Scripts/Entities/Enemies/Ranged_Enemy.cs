﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Ranged_Enemy : Enemy
{
    public float m_FleeRange = 5f;
    public float m_ShootingDistance = 10f;
    public float m_AvoidanceRange = 1f;
    public float m_MovementStep = 0.03f;
    public float m_FireDelay = 1.5f;
    [SerializeField] GameObject m_ArrowSpawnPoint = null;
    public ArrowData m_ArrowData;

    //private void Awake()
    //{
    //    //fleeSquared = fleeRange * fleeRange;
    //}

    // Update is called once per frame
    override protected void Update()
    {
        if (State != EnemyStates.Dead)
        {
            base.Update();

            Vector3 direction = m_player.transform.position - transform.position;
            if (Vector3.SqrMagnitude(direction) < m_FleeRange * m_FleeRange)
            {
                State = EnemyStates.FleeFromPlayer;
            }
            else if (Physics.Raycast(transform.position, direction, out RaycastHit hit, m_ShootingDistance))
            {
                if (hit.collider.tag == "Player")
                {
                    State = EnemyStates.Attack;
                }
            }
            else
            {
                State = EnemyStates.MoveTowardsPlayer;
            }
            Move();
        }
    }

    public override void Move()
    {
        base.Move();

        switch (State)
        {
            case EnemyStates.FleeFromPlayer:
                {
                    m_agent.isStopped = true;
                    m_RunEvent.Invoke();
                    Vector3 direction = transform.position - m_player.transform.position;
                    direction += CalculateAvoidance(m_AvoidanceRange);
                    direction.Normalize();
                    Vector3 point = m_player.transform.position + direction * m_FleeRange / 1.5f;
                    transform.position = Vector3.MoveTowards(transform.position, point.normalized, m_MovementStep / 2f);
                }
                break;
            case EnemyStates.MoveTowardsPlayer:
                {
                    m_agent.isStopped = false;
                    m_RunEvent.Invoke();
                    Vector3 direction = CalculateAvoidance(m_AvoidanceRange);
                    transform.position = Vector3.MoveTowards(transform.position, transform.position + direction.normalized, m_MovementStep);
                    if (m_agent.enabled && NavMesh.SamplePosition(transform.position, out NavMeshHit hit, 3f, NavMesh.AllAreas))
                    {
                        m_agent.SetDestination(m_player.transform.position);
                    }
                }
                break;
            case EnemyStates.Attack:
                {
                    m_agent.isStopped = true;
                    m_IdleEvent.Invoke();
                    Vector3 direction = CalculateAvoidance(m_AvoidanceRange);
                    transform.position = Vector3.MoveTowards(transform.position, transform.position + direction.normalized, m_MovementStep);
                }
                Attack();
                break;
        }
    }

    public override bool Setup(EnemyContainerBase dataContainer)
    {
        base.Setup(dataContainer);
        return true;
    }

    private void OnEnable()
    {
        State = EnemyStates.MoveTowardsPlayer;
    }

    protected override void Attack()
    {
        base.Attack();

        if (!m_isAttacking)
        {
            m_AttackEvent.Invoke();           
        }
    }

    //private IEnumerator Shooting()
    //{
    //    float timer = 0f;
    //    isAttacking = true;
    //    while (true)
    //    {
    //        timer += Time.deltaTime;
    //        if (timer >= fireDelay)
    //        {
    //            Shoot();
    //            isAttacking = false;
    //            yield break;
    //        }
    //        yield return null;
    //    }
    //}

    public void Shoot()
    {
        if (m_ArrowData == null)
        {
            Debug.LogError("Ranged enemy arrowData is unsupplied");
            return;
        }
        Arrow arrow = Instantiate(m_ArrowData.Prefab, m_ArrowSpawnPoint.transform.position + transform.forward * 1f, Quaternion.identity);
        arrow.transform.LookAt(m_player.transform);
        arrow.StartCoroutine(arrow.ArrowFire(m_ArrowData));
    }

    override protected void Death()
    {
        base.Death();
    }

    public void EndAttack()
    {
        m_isAttacking = false;
    }
}
