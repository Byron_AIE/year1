﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    public GameObject prefab;
    public int pre_generated_count = 3;

    public bool hard_limit;
    public int grow_rate;

    private GameObject[] generated;
    public int used_count;

    private void Awake()
    {
        generated = new GameObject[pre_generated_count];
        for (int i = 0; i < generated.Length; ++i)
        {
            generated[i] = Instantiate(prefab);
            generated[i].SetActive(false);
        }
        used_count = 0;
    }


    public GameObject Create(Vector3 position, Quaternion rotation)
    {
        GameObject result = null;

        Debug.Log(used_count);

        // If there is space in the pool
        if (used_count < generated.Length)
        {
            result = generated[used_count];
            result.SetActive(true);
           
            result.SendMessage("Awake");
            result.SendMessage("Start");
                     
            result.transform.position = position;
            result.transform.rotation = rotation;

            used_count++;
        }

        return result;
    }


    public void Free(GameObject gameobject)
    {
        for (int i = 0; i < used_count; ++i)
        {
            if (generated[i] == gameobject)
            {
                --used_count;

                gameobject.SendMessage("OnDestroy");
                gameobject.SetActive(false);
                GameObject temp = generated[i];
                generated[i] = generated[used_count];
                generated[used_count] = temp;

                return;
            }
        }
    }
}
