﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySound : MonoBehaviour
{
    AudioManager audioManager;

    public AudioSource audioSource;

    [SerializeField] string accessString;
    [SerializeField] float volume = 1f;

    void Start()
    {
        audioManager = FindObjectOfType<AudioManager>();
    }

    public void PlaySoundFromEvent(string accessString) //Use this when playing a sound FROM an animation event.
    {
        audioSource.PlayOneShot(audioManager.GetRandomSound(accessString), volume);
    }

    public void PlaySoundFromCall() //Use this when playing a sound NOT FROM an animation event.
    {
        audioSource.PlayOneShot(audioManager.GetRandomSound(accessString), volume);
    }
}
