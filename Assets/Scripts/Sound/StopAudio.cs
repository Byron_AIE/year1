﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopAudio : MonoBehaviour
{
    [SerializeField] AudioSource audioSource;

    public void StopAudioSource()
    {
        Debug.Log("stopaudio");
        audioSource.Stop();
    }

}
