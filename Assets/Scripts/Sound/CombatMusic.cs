﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatMusic : MonoBehaviour
{
    [SerializeField] AudioSource combatMusicAS;
    [SerializeField] Animator audioAnim;

    AudioManager audioManager;

    public bool playCombatMusic = false;
    public bool dmgTakenThisWave = false;


    void Start()
    {
        audioManager = FindObjectOfType<AudioManager>();
    }

    void Update()
    {
        if (combatMusicAS.isPlaying == false && playCombatMusic)
        {
            PlayCombatMusic("CombatMusic");
            FadeIn();
        }

        else if (!playCombatMusic)
        {
            combatMusicAS.Stop();
        }
    }

    public void PlayCombatMusic(string accessString)
    {
        combatMusicAS.PlayOneShot(audioManager.GetUniqueSound(accessString));
    }

    public void FadeOut()
    {
        Debug.Log("Fading Out");
        audioAnim.SetTrigger("FadeOut");
    }

    public void FadeIn()
    {
        Debug.Log("Fading In");
        audioAnim.SetTrigger("FadeIn");
    }

    public void ResetBool()
    {
        dmgTakenThisWave = false;
        playCombatMusic = false;
    }
}
