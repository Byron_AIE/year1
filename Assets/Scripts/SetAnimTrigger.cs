﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetAnimTrigger : MonoBehaviour
{
    [SerializeField] Animator targetAnimator;
    [SerializeField] string trigger;

    [SerializeField] Animator bowAnim = null;
    [SerializeField] Animator swordAnim = null;
    [SerializeField] Animator sprayAnim = null;

    [SerializeField] MeshRenderer swordMesh = null;
    [SerializeField] SkinnedMeshRenderer swordMesh1 = null;

    [SerializeField] SkinnedMeshRenderer bowMesh = null;
    [SerializeField] MeshRenderer arrowMesh = null;
    [SerializeField] SkinnedMeshRenderer bowMesh2 = null;

    [SerializeField] SkinnedMeshRenderer sprayMesh = null;
    [SerializeField] SkinnedMeshRenderer sprayMesh1 = null;
    [SerializeField] SkinnedMeshRenderer sprayMeshArrows = null;

    public void PlayAnimation(string animTrigger)
    {
        targetAnimator.SetTrigger(animTrigger);
        //Debug.Log("Animation string: " + animTrigger);
    }

    public void SetAnimBoolFalse(string animBool)
    {
        targetAnimator.SetBool(animBool, false);
    }

    public void SetAnimBoolTrue(string animBool)
    {
        targetAnimator.SetBool(animBool, true);
    }

    public void SetTrigger()
    {
        targetAnimator.SetTrigger(trigger);
    }

    public void BowDraw()
    {
        //Debug.Log("BowDraw");
        bowAnim.SetTrigger("BowDraw");
    }

    public void BowSheathe()
    {
        bowAnim.SetTrigger("BowSheathe");
    }

    public void SwordDraw()
    {
        swordAnim.SetTrigger("SwordDraw");
    }

    public void SwordSheathe()
    {
        swordAnim.SetTrigger("SwordSheathe");
    }

    public void BowSheathed()
    {
        bowAnim.Play("BowSheathed 0", 0, 1);
    }

    public void SwordSheathed()
    {
        swordAnim.Play("SwordSheathed", 0, 1);
    }

    public void HideBow()
    {
        bowMesh.enabled = false;
        arrowMesh.enabled = false;
        bowMesh2.enabled = false;
    }

    public void ShowBow()
    {
        bowMesh.enabled = true;
        arrowMesh.enabled = true;
        bowMesh2.enabled = true;
    }

    public void HideSword()
    {
        swordMesh.enabled = false;
        swordMesh1.enabled = false;
    }

    public void ShowSword()
    {
        swordMesh.enabled = true;
        swordMesh1.enabled = true;
    }

    public void ResetBow()
    {
        bowAnim.Play("BowIdle", 0, 1);
        bowAnim.WriteDefaultValues();
    }

    public void ResetSword()
    {
        swordAnim.Play("SwordIdle", 0, 1);
        swordAnim.WriteDefaultValues();
    }

    public void ResetSpray()
    {
        sprayAnim.Play("Idle", 0, 1);
        sprayAnim.WriteDefaultValues();
    }

    public void HideSpray()
    {
        sprayMesh.enabled = false;
        sprayMesh1.enabled = false;
        sprayMeshArrows.enabled = false;
    }

    public void ShowSpray()
    {
        sprayMesh.enabled = true;
        sprayMesh1.enabled = true;
        sprayMeshArrows.enabled = true;
    }

    public void HideArrows()
    {
        sprayMeshArrows.enabled = false;
    }

    public void ShowArrows()
    {
        sprayMeshArrows.enabled = true;
    }

    public void SpraySheathed()
    {
        sprayAnim.Play("SpraySheathed", 0, 1);
    }

    public void SprayDraw()
    {
        sprayAnim.SetTrigger("SprayDraw");
    }

    public void BowFromSpray()
    {
        bowAnim.SetTrigger("SwapFromSpray");
    }
}
