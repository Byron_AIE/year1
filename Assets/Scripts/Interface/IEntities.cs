﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEntities<T>
{
    void TakeDamage(T damage);
}