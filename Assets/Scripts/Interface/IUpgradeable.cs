﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUpgrade
{
    bool Apply(ref UpgradeData data);    
}