﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SendMessage : MonoBehaviour
{
    public GameObject[] targetObjects;

    //[SerializeField] string[] messagesToSend;

    public void SendMessageToTarget(string message)
    {
        foreach (GameObject targetObject in targetObjects) //(int i = 0; i < targetObjects.Length; i++)
        {
            targetObject.SendMessage(message, SendMessageOptions.DontRequireReceiver);
            //Debug.Log("sent " + message);
        }      
    }
}