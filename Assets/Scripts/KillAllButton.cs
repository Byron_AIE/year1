﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillAllButton : MonoBehaviour
{
    public KeyCode key;
    private WaveManager waveManager;

    void Start()
    {
        waveManager = GameManager.Wavemanager;
    }

    // Update is called once per frame
    void Update()
    {
#if UNITY_EDITOR
        if (Input.GetKeyDown(key))
        {
            waveManager.KillAllEnemies();
        }
#endif
    }
}
