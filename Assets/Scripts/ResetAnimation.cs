﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetAnimation : MonoBehaviour
{
    [SerializeField] Animator targetAnimator;
    [SerializeField] string defaultState;

    public void ResetAnim()
    {

        //Debug.Log("WriteDefaults");
        targetAnimator.Play(defaultState, 0, 1);
        targetAnimator.WriteDefaultValues();
    }

}
